#SPC
{
	#author "Camerin"
	#comment "Desert ruins theme 3"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
	#sky_and_dream
}

#0 w160 t49

$EF $FF $46 $46
$F1 $02 $32 $01

#0 @11 v245 $ED $1C $AA p20,40
o2[g1]7
[r1]3
g1
g2f2
g2d2
g2f2
d2g2
g2f2
g2d2
g2f2
d+2d2
d+2d2
d+2g2
d2g2
d2g2
d+2d2
f+2g2
d+1
d1
[g1]6
;
#1 @4 v190
o3[r1]4
g1
g+1
g1
[r1]4
g2f2
g2d2
g2f2
d2g2
g2f2
g2d2
g2f2
d+2d2
d+2d2
d+2g2
d2g2
d2g2
d+2d2
f+2g2
d+1
d1
[r1]6
;
#2 @4 v190
r16  ^16^8^4^2
[r1]3
d1
d2c2
d1
[r1]4
d2c2
d2<a2
>d2c2
<a2>d2
d2c2
d2<a2
>d2c2
<a+2a2
a+1
a+2>d2
<a2>d2
<a2>d2
<a+1
a2>d2
<a+1
a1
[r1]6
;
#4 @12 v200
o5[r1]7
[@12g16@12g16r16@12g16@12g16r16@12g16@12g16r16@12g16@12g16r16@12g16@12g16@12g16@12g16]20
[r1]6
;
#3 @3 v240
o2[r1]9
[g16a16a+16>c16d16c16<a+16a16]4
o4 @11 v160 $ED $1C $AA p20,40
>d8^16<a+8^16g8>c8^16<a8^16f8
>d8^16<a+8^16g8>d8^16<a8^16f8
>d8^16<a+8^16g8>c8^16<a8^16f8
>d8^16<a+8^16g8f8a8g4
v200
>d16<a+16g16>d16<a+16g16>d16<a+16>c16<a16f16>c16<a16f16>c16<a16
>d16<a+16g16>d16<a+16g16>d16<a+16>d16<a16f16>d16<a16f16>d16<a16
>d16<a+16g16>d16<a+16g16>d16<a+16>c16<a16f16>c16<a16f16>c16<a16
a+16g16d+16a+16g16d+16a+16g16a16f+16d16a16f+16d16a16f+16
d+8g8a+8>d+8<d8f8a+8>d8
[<d8f8a8>c8<d8g8a+8>d8]3
<d+8g8a+8>c8<d8f8a+8>d8
<d8f+8a8>c8<d8g8a+8>d8
<d+8g8a+8>d+8f8d+8<a+8g8
d8f+8a8>c8d8c8<a8f+8
[r1]6
;
#5 @6 v220
o4[r1]11
@6 $ED $1F $88 p0,0
g8^16f16g8d8f8e8d8c8
<a+8^16>c16d16c16<a+16>d16<a8f8d8f8
>g8^16f16g8d8f8e8d8c8
<a+16>c16d16<a+16a16g16f16a16>c16<a16f16d16g4
>g8^16f16g8d8f8e8d8c8
<a+8^16>c16d16c16<a+16>d16<a8f8d8f8
>g8^16f16g8d8f8e8d8c8
<g16f16d+16f16g16a16a+16>c16d8^16d+16d4
@0 $ED $5A $E3 p5,10
d+2d+4d4
c4<a4a+8a8g4
d4f4g4d4
f4d4g2
>d+4f4d+4d4
c4<a4a+8a8g4
a4^8a+8>c4<a+8>c8
d1
@4 v190 p40,35 $ED $2C $88
<g2^8g+8b8>c8
d16d+16d2d+8d8c8
<b32>c32<b32>c32<b32>c32<b32>c32<b4^8>c8d8d+16f+16
g4b4g+4>c4
<g4b4g+4>c4
[d32d+32]8d2
;
#6 @6 v220
o4[r1]15
@6 $ED $1F $88 p0,0
a+8^16a16a+8g8a8g8f8e8
d8^16e16f16e16d16f16c8<a8g8a8
>a+8^16a16a+8g8a8g8f8e8
<a+16a16g16a16a+16>c16d16c16<g4f+4
@0 $ED $5A $E3 p5,10
>g4^8a8a+4f4
d+4f4d+4d4
c4d4c4<a+4
>c4<a4a+2
>g4^8a8a+4f4
g4f+8a8g4d4
d+8d8c8d8d+8f8g8a8
g2f+2
[r1]3
@4 v190 p40,35 $ED $2C $88
[d1]2
<g8g+8b8>c8d2
;
#7 @12 v240
o3[r1]2
[@12e8^16@12a16@12b16@12e16@12a16@12b16@12e16@12a16@12b16@12e16@12e8@12b8]5
[@12e16@12a16@12b16@12e16@12a16@12b16@12e16@12a16@12e8^16@12b16@12b16@12a16@12a16@12b16]20
[r1]6
;
                

#amk=1
