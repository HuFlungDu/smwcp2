lorom                   ;\ ROM is LoRom
header                  ;/ and has a header.


!DisableRAM = $79			;Bitwise, xxxxxcCF
						;x = unused, c = Disable Cape Floating, C = Disable Cape, F = Disable Firepower

org $00D081
autoclean	JML ReplaceFire				;This probably isn't necessary, but it doesn't hurt

org $00D062
autoclean	JML ReplaceCapeSpin

org $00D8FF
autoclean	JML ReplaceCapeFloat

org $00D90D|$800000
	CapeFloatReturnPositive:

org $00D904|$800000
	CapeFloatReturnNegative:

org $00D924|$800000
	CapeFloatReturnNoFloat:


freecode

ReplaceFire:
		CMP #$03
		BNE NoFireball
		LDA !DisableRAM
		BIT #$01
		BNE NoFireball
		JML $80D085
		NoFireball:
		JML $80D0AD

ReplaceCapeSpin:
		LDA $19
		CMP #$02
		BNE ReplaceFire
		LDA !DisableRAM
		BIT #$02
		BNE NoCape
		JML $80D068
		NoCape:
		JML $80D0AD

ReplaceCapeFloat:
	lda !DisableRAM
	bit #$04
	beq .dont_replace
		jml CapeFloatReturnNoFloat
	.dont_replace
		lda $14A5
		bne +
			jml CapeFloatReturnNegative
		+
		jml CapeFloatReturnPositive
