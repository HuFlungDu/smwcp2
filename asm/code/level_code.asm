;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;True LevelASM Code lies ahead.
;If you are too lazy to search for a level
;Use CTRL+F. The format is as following:
;levelx - Levels 0-F
;levelxx - Levels 10-FF
;levelxxx - Levels 100-1FF
;Should be pretty obvious...
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!useParallaxScroll = $7FFEFF
!FreeRAM = $7F9000
!cameraY = $20

level0:
level1:
level2:
level3:
level4:
level5:
level6:
level7:
level8:
level9:
levelB:
levelD:
levelE:
levelF:
level12:
level14:
level15:
level19:

level20:
level21:
level23:
level24:
level25:
level26:
level27:
level28:

level2B:

level2C:
level2E:
level2F:
level30:
level31:
level32:
level33:
level34:
level36:
level37:
level38:
level39:
level3B:
level3C:
level3D:
level3E:
level3F:
level40:
level41:

level44:

level49:
level4B:
level4C:
level4D:
level4E:
level4F:
level50:
level51:
level52:
level53:
level55:
level56:
level5C:
level5D:
level5E:
level5F:
level60:
level61:
level62:
level63:
level64:
level65:
level66:
level67:
level69:
level6A:
level6B: 
level6C:
level6D: 


levelC8:
level76:
level77:
level7A:
level7B:
level7D:
level7E:
level7F:
level80:
level81:
level84:
level8B:
level8D:
level8E:
level8F:
level90:
level92:
level93:
level94:
level95:
level96:
level97:
level98:
level99:
level9A:
level9B:
level9C:
level9D:
level9E:
level9F:
levelA0:
levelA4:
levelA7:
levelA8:
levelAB:
levelAD:
levelAE:
levelAF:
levelB0:
levelB1:
levelB2:
levelB3:
levelB4:
levelB5:
levelB6:
levelB7:
levelB8:
levelB9:
levelBA:
levelBC:
levelBD:
levelBF:
levelC0:
levelC1:
levelC2:
levelC3:
levelC4:

levelC5:
levelC6:
levelC9:
levelCA:
levelCB:
levelCC:
levelCD:
levelCE:
levelCF:
levelD0:
levelD1:
levelD2:
levelD3:
levelD4:
levelD5:
levelD6:
levelD7:
levelD8:
levelD9:
levelDA:
levelDB:
levelDC:

levelDE:
levelDF:
levelE2:
levelE4:

levelE6:
levelE7:
levelE8:

levelEC:
levelF0:
levelF3:
levelF4:
levelF5:
levelF6:
levelF7:
levelF8:
levelF9:
levelFA:
levelFD:
levelFE:
levelFF:
level100:

level106:
level107:
level108:
level109:
level10A:
level10B:

level10D:
level10E:


level110:
level111:
level113:
level114:
level115:
level116:
level118:
level119:
level11A:
level11B:
level11D:
level11E:
level11F:
level122:

level124:

level126:
level127:

level129:
level12A:
level12C:
level12F:
level130:
level131:
level132:
level133:
level134:
level135:
level137:
level139:
level13A:
level13B:
level13E:
level140:
level141:
level143:

level14F:
level150:
level151:
level152:
level153:
level154:
level155:
level156:
level157:
level158:
level159:
level15A:
level15B:
level15C:
level15D:
level15E:
level15F:
level160:
level161:
level162:
level163:
level164:
level165:
level167:
level168:
level169:
level16A:
level16B:
level16C:
level16D:
level16E:
level16F:
level173:
level174:
level175:
level176:
level177:
level178:
level17B:
level17C:
level17D:
level17E:
level17F:
level180:
level181:

level183:
level184:
level185:
level186:
level188:
level189:
level18A:
level18B:
level18C:
level18D:
level18E:
level18F:
level190:
level191:
level192:
level193:
level194:
level195:
level196:
level197:
level198:
level199:
level19A:
level19B:
level19C:
level1A6:
level1A9:

level1AC:
level1AD:
level1AE:
level1AF:
level1B0:
level1B1:
level1B2:
level1B3:
level1B4:
level1B5:
level1B6:
level1B7:
level1B8:
level1B9:
level1BA:
level1BB:
level1BC:
level1BD:
level1BE:
level1BF:
level1C0:
level1C1:
level1C2:
level1C3:
level1C4:
level1C5:
level1C6:
level1C7:
level1C8:
level1CA:
level1CB:
level1CC:
level1CD:
level1CE:
level1CF:
level1D0:
level1D1:

level1D2:
level1D3:
level1D4:
level1D5:
level1D6:
level1D7:

level1D9:
level1DA:
level1DC:
level1DE:
level1DF:
level1E0:
level1E1:
level1E2:
level1E3:
level1E4:
level1E5:
level1E7:
level1E8:
level1E9:
level1EA:
level1EB:
level1EC:
level1ED:
level1EE:
level1EF:
level1F0:
level1F1:
level1F2:
level1F3:
level1F4:
level1F5:
level1F6:
level1F7:
level1F8:
level1F9:
level1FA:
level1FB:
level1FC:
level1FD:
level1FE:
level1FF:
	RTS

; -------------------------------------------


level128:
	; REP #$20
	; LDA.w #.parallax_table		
	; STA $00					
	; SEP #$20			
	; LDA.b #.parallax_table>>16	
	; STA $02	
	; LDA #$01
	; STA !useParallaxScroll
	; JMP DoParallaxScrollingW
	
; .parallax_table
	; dw $0000, $0020
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $007F-$28, $0004
	; dw $00A6-$28, $0004
	; dw $00A6-$28, $0004
	; dw $00A6-$28, $0004
	; dw $00A6-$28, $0004
	; dw $00A6-$28, $0004
	; dw $00A6-$28, $0004
	; dw $00A6-$28, $0004
	; dw $00A6-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	; dw $00C8-$28, $0004
	
	; lmao
	
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00EF-$28, $0004
	; dw $00FE-$28, $0004
	; dw $00FE-$28, $0004
	; dw $00FE-$28, $0004
	; dw $00FE-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0129-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $0177-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $017B-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0180-$28, $0004
	; dw $0188-$28, $0004
	; dw $0188-$28, $0004
	; dw $0188-$28, $0004
	; dw $0188-$28, $0004
	; dw $0188-$28, $0004
	; dw $0188-$28, $0004
	; dw $0188-$28, $0004
	; dw $0188-$28, $0004
	; dw $0188-$28, $0004
	; dw $0188-$28, $0004
	
; .parallax_table
; dw $0000, $0020	; this is layer 2
; dw $007F, $0038	; from here on (I think) you gotta subtract the first/position values by 28 because [explanation]
; dw $00A6, $0020
; dw $00C8, $0030
; dw $00EF, $0050
; dw $00FE, $0010
; dw $0129, $0040
; dw $0177, $003C	; this is where the lava starts so this is the layer 3 part
; dw $017B, $0038
; dw $0180, $0030
; dw $0188, $0028


; DoParallaxScrollingW:
	; REP #$20			
	; LDA $00
	; CLC				
	; ADC #$0002			
	; STA $03				
	; CLC				
	; ADC #$0002			
	; STA $06				
						
	; SEP #$20
	; LDA $02
	; STA $05				
	; STA $08				
	
	; LDA $14
	; LSR
	; LSR
	; STA $09
	
	; ; prev multiplier
	; STZ $0A
	; STZ $0B
	; ; prev xpos
	; STZ $0C
	; STZ $0D
	
	
	; JSR DoScrollStuff
	; PHB
	; PHK
	; PLB
	; LDX #$00			; X = index to the HDMA table in RAM
	; LDY #$00			; Y = index to the offset table
					; ; We don't need to load #$0000 since when X and Y
	
	
; .loop
	; REP #$30			;
	; LDA [$00],y			; \
	; BMI .exitLoop			; | (Negative values are table ends)
	; SEC				; | If the current y offset
	; SBC !cameraY			; | Is above the screen or at the screen
	; BMI .base			; | Then set the first scanline and beyond to that y position's x offset
	; BEQ .base			; /

	; INX				; \ Increase the HDMA table position
	; INX				; |
	; INX				; /

	; LDA [$03],y			; \

	; JSR DoCalculationW		; | Get the offset and do the requested calculation with it
	; STA !FreeRAM+1,x		; / And store it to the current slot in the HDMA table
	
	; LDA [$06],y			; \
	; SEC				; | Get the difference in scanline counts.
	; SBC [$00],y			; | And store it to the current slot in the HDMA table.
	; SEP #$20			; |
	; STA !FreeRAM,x			; /
	
	
	; INY				; \
	; INY				; | Increase the offset table position
	; INY				; |
	; INY				; /
	; BRA .loop			;
					; ;
; .base					;
	; LDA [$03],y			; \
	; JSR DoCalculationW		; | Get the offset and do the requested calculation with it
	; STA !FreeRAM+1,x		; / And store it to the current slot in the HDMA table
	
	; LDA [$06],y			; \
	; SEC				; | Get the difference in scanline counts.
	; SBC !cameraY			; | But only if it's positive
	; BMI +				; |
	; SEP #$20			; |
	; STA !FreeRAM,x			; /

; +
	; INY				; \
	; INY				; | Increase the offset table position
	; INY				; | We don't increase the offset table position,
	; INY				; / since we might jump here more than once.
	; BRA .loop			;

; .exitLoop
	; SEP #$20
	; LDA #$7F
	; STA !FreeRAM,x
	; LDA !FreeRAM-2,x
	; STA !FreeRAM+1,x
	; LDA !FreeRAM-1,x
	; STA !FreeRAM+2,x
	; LDA #$00
	; STA !FreeRAM+3,x
	; SEP #$10
	
	; PLB
	; RTS
	
; DoCalculationW:
	; CMP $0A
	; BNE .new_value
	; LDA $0C
	; BRA .skip_calc
	
; .new_value
	; STA $0A

	; CMP #$0100
	; BEQ +
	; SEP #$20
	; STA $4206
	; REP #$20
	; LDA $1A
	; ASL
	; ASL
	; ASL
	; STA $4204
	; NOP #8
	; LDA $4214
	; BRA ++
; +
	; LDA #$0000
; ++
	; STA $0C
	
; .skip_calc
	; STA $0E
	
	; ; get an in range index to the wave table
	; TYA
	; LSR
	; LSR
	; CLC
	; ADC $09
	; AND #$003F

	; PHY
	; ASL
	; TAY
	; LDA .wave_table,y
	; CLC
	; ADC $0E
	; PLY
	; RTS
	
; .wave_table:
	; dw   0,   0,   1,   1,   1,   1,   2,   2
	; dw   2,   2,   2,   2,   3,   3,   3,   3
	; dw   3,   3,   3,   3,   2,   2,   2,   2
	; dw   2,   2,   1,   1,   1,   1,   0,   0
	
	; dw  -0,  -0,  -1,  -1,  -1,  -1,  -2,  -2
	; dw  -2,  -2,  -2,  -2,  -3,  -3,  -3,  -3
	; dw  -3,  -3,  -3,  -3,  -2,  -2,  -2,  -2
	; dw  -2,  -2,  -1,  -1,  -1,  -1,  -0,  -0
	
	
	

; -------------------------------------------


level35:
	LDA $71
	ORA $9D
	ORA $13D4	
	BNE .return
	
	LDA $14
	LSR
	BCS .return
	
	LDA $95
	ASL
	TAX
	REP #$20
	LDA .tide_ypos,x
	CMP $24
	BEQ .skip_move
	BPL .move_up
	
.move_down
	DEC $24
	BRA .skip_move
	
.move_up
	INC $24
	
.skip_move
	SEP #$20
.return
	RTS
	
; for reference: 0040 gives you low tide
.tide_ypos:
	dw $0070	; 00
	dw $0070	; 01
	dw $0040	; 02
	dw $0080	; 03
	dw $0000	; 04
	dw $0000	; 05
	dw $0080	; 06
	dw $00A0	; 07
	dw $0090	; 08
	dw $0000	; 09
	dw $0000	; 0A
	dw $0050	; 0B
	dw $0060	; 0C
	dw $0060	; 0D
	dw $0060	; 0E
	dw $0090	; 0F

; -------------------------------------------

level1E:
	LDA $71
	ORA $9D
	ORA $13D4	
	BNE .return
	
	LDA $14
	LSR
	BCS .return
	
	LDA $95
	ASL
	TAX
	REP #$20
	LDA .tide_ypos,x
	CMP $24
	BEQ .skip_move
	BPL .move_up
	
.move_down
	DEC $24
	BRA .skip_move
	
.move_up
	INC $24
	
.skip_move
	SEP #$20
.return
	RTS
	
; for reference: 0040 gives you low tide
.tide_ypos:
	dw $0060	; 00
	dw $0060	; 01
	dw $0040	; 02
	dw $0080	; 03
	dw -$0000	; 04
	dw -$0000	; 05
	dw $0080	; 06
	dw $00A0	; 07
	dw $0090	; 08
	dw -$0000	; 09
	dw -$0000	; 0A
	dw $0050	; 0B
	dw $0060	; 0C
	dw $0060	; 0D
	dw $0060	; 0E
	dw $0090	; 0F

	
; -------------------------------------------
	
!scroll_state = $1442
!scroll_timer = $1443
	
level57:
	LDA $71
	ORA $9D
	ORA $13D4	
	BNE .return
	
	LDA !scroll_state
	TAX
	LDA $14
	AND .freq,x
	BNE .return
	
	TXA
	ASL
	TAX
	REP #$20
	LDA $1464
	CLC
	ADC .speed,x
	STA $1464
	
	DEC !scroll_timer
	SEP #$20
	BPL .return
	
	LDA !scroll_state
	INC
	AND #$03
	STA !scroll_state
	ASL
	TAX
	REP #$20
	LDA .time,x
	STA !scroll_timer
	SEP #$20
	
.return:
	RTS
	
; positive ($0001) up, negative ($FFFF) down
.speed:
	dw $FFFF, $0000, $0001, $0000
	
; fast 00 > 01 > 03 > 07 > 0f > 1f > 3f > 7f > ff slow
.freq:
	db $03, $00, $03, $00
	
; (if speed is 1 or -1, it's how many pixels you move)
.time:
	dw $0030, $0020, $0030, $0020
	
; -------------------------------------------

	
level166:
	STZ $1411
	RTS
	
level10F:
	REP #$20			
	LDA.w #.parallax_table		
	STA $00					
	SEP #$20			
	LDA.b #.parallax_table>>16	
	STA $02	
	LDA #$01
	STA !useParallaxScroll
	JMP DoParallaxScrolling

.parallax_table
dw $0000, $0030
dw $007F-$28, $0030
dw $00FE-$28, $0030
dw $0138-$28, $002E
dw $015D-$28, $0028
dw $0169-$28, $0024
dw $016F-$28, $0022
dw $0173-$28, $0020
dw $017B-$28, $0018
dw $0186-$28, $0014
dw $0194-$28, $0012
dw $01A5-$28, $0010

level102:
level104:
	REP #$20			
	LDA.w #.parallax_table		
	STA $00					
	SEP #$20			
	LDA.b #.parallax_table>>16	
	STA $02	
	LDA #$01
	STA !useParallaxScroll
	JMP DoParallaxScrolling

.parallax_table:
dw $0000, $0010
dw $007F, $0010
dw $00E4-$28, $0040
dw $0127-$28, $0020
dw $0146-$28, $0010
dw $0190-$28, $000C	
	
level105:
	REP #$20			
	LDA.w #.parallax_table		
	STA $00					
	SEP #$20			
	LDA.b #.parallax_table>>16	
	STA $02	
	LDA #$01
	STA !useParallaxScroll
	JMP DoParallaxScrolling

.parallax_table
	dw $0000, $0048
	dw $007F-$28, $0020
	dw $0096-$28, $0038
	dw $00AB-$28, $0048
	dw $00F8-$28, $0040
	dw $0107-$28, $0038
	dw $0126-$28, $0030
	dw $0137-$28, $0028
	dw $014E-$28, $0020
	dw $016F-$28, $0018
	dw $01A1-$28, $0010
	dw $01A1, $0010
	
level29:
level2A:
level125:
	REP #$20			
	LDA.w #.parallax_table		
	STA $00					
	SEP #$20			
	LDA.b #.parallax_table>>16	
	STA $02	
	LDA #$01
	STA !useParallaxScroll
	JMP DoParallaxScrolling

.parallax_table
	dw $0000, $0040
	dw $007F, $0040
	dw $00D5-$28, $0034
	dw $00F7-$28, $0028
	dw $0105-$28, $0020
	dw $0127-$28, $0018
	dw $0167-$28, $000E
	dw $01A1-$28, $000A
	dw $01A1, $0010

levelA:
levelC:
level54:
level58:
level68:
level79:
levelA5:
levelA6:
level12B:
level1DB:
level14B:
level1DD:
	STZ $00
	STZ $01
	JMP sub_layer_3_scroll

level11C:
level13C:
level13D:
level13F:
 	LDA #$60
	STA $00
	LDA #$FF
	STA $01
	;LDA #$FF
	;STA $22
	;STZ $01
	JMP sub_layer_3_scroll

level18:
levelF1:
levelF2:
	REP #$20			
	LDA.w #.parallax_table		
	STA $00					
	SEP #$20			
	LDA.b #.parallax_table>>16	
	STA $02	
	LDA #$01
	STA !useParallaxScroll
	JMP DoParallaxScrolling

.parallax_table
	dw $0000, $0010
	dw $007F, $0010
	dw $00F6-$28, $0100
	dw $0110-$20, $0050
	dw $0126-$28, $0058
	dw $0137-$28, $0048
	dw $013F-$28, $0040
	dw $017E-$28, $0038
	dw $0183-$28, $0030
	dw $0186-$28, $0028
	dw $018B-$28, $0100
	dw $0168,$0100
	dw $01E2,$0100



; put all the levels with wavy hdma here
level5A:
level4A:
level6F:
level91:
levelE3:
levelEA: 
levelEB: 
levelED: 
level1D8:
level42:
level43:

level1C:
 	REP #$20			;\ Set processor 16 bit
	LDA #$0D02			;| $4330 = $210D
	STA $4330			;| $4331 = Mode 02
	LDA #$9E00			;| 
	STA $4332			;| Destination: $7F9E00
	LDY #$7F			;| (low and high byte)
	STY $4334			;| 
	LDA #$0F02			;| $4340 = $210F
	STA $4340			;| $4341 = Mode 02
	LDA #$9E00			;| 
	STA $4342			;| Destination: $7F9E00
	LDY #$7F			;| (low and high byte)
	STY $4344			;| 	
	SEP #$20			;/ Set processor 8 bit

	LDA #$18			;\ Enable HDMA
	TSB $0D9F			;/ on channel 3,4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;HDMA Main Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	LDX #$00			;\ Init of
	LDY #$00			;/ X and Y

	SEP #$20			;\ Set processor 8 bit
	LDA $13				;| Set speed of waves
	LSR A				;| Adding more LSR A
	LSR A				;| will make it slower
	STA $00				;/ Store in scratch RAM

	PHB					;\ Push data bank
	PHK					;| Push program bank
	PLB					;/ Pull data bank

AWave_Loop:
	LDA #$06			;\ Set scanline height
	STA $7F9E00,x		;| for each wave
	TYA					;| Transfer Y to A
	ADC $00				;| Add in scratch RAM
	AND #$0F			;| and transfer #$0F bytes
	PHY					;| Push Y
	TAY					;/ Transfer A to Y

	LDA.w AWave_Table,y	;\ Load in wave values
	LSR A				;| half of waves only
	CLC					;| Clear carry flag
	ADC $1462			;| Apply to layer 1
	STA $7F9E01,x		;| X position low byte
	LDA $1463			;| And add nothing to
	ADC #$00			;| layer 1 X position
	STA $7F9E02,x		;/ high byte

	LDA.w AWave_Table,y	;\ Load in wave values
	CLC					;| Clear carry flag
	ADC $1466			;| Apply to layer 2
	STA $7F9E03,x		;| X position low byte
	LDA $1467			;| And add nothing to
	ADC #$00			;| layer 2 X position
	STA $7F9E04,x		;/ high byte

	PLY					;\ Pull Y
	CPY #$25			;| Compare with #$25 scanlines
	BPL AEnd_Wave		;| If bigger, end HDMA
	INX					;| Increase X
	INX					;| Increase X
	INX					;| Increase X
	INY					;| Increase Y
	BRA AWave_Loop		;/ Do the loop

AEnd_Wave:
	PLB					;\ Pull data bank
	LDA #$00			;| End HMDA by writing
	STA $7F9E05,x		;| #$00 here
	RTS			;/ Return
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Table Settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

AWave_Table:
	db $00
	db $01
	db $02
	db $03
	db $04
	db $05
	db $06
	db $07
	db $07
	db $06
	db $05
	db $04
	db $03
	db $02
	db $01
	db $00


level45: 
	REP #$10	;floor generator
	LDX $80
	CPX #$00D8	;increase the 00C8 if mario is still too visible
	BCC +
	CPX #$0200
	BCS +
	STZ $7B
	+
	SEP #$10
	RTS
	

!FreeRamForMode2 = $7F9D00	;Change if it conflicts with other patches (about $40 bytes)

; Needed to get the graphics correct.
incbin "Blimp_battlefield.bin" -> BlimpBattlefield
incbin "Background.bin" -> BlimpBackground

level6E: 
LDA $1493
CMP #$F8
BNE +
LDA #$09
STA $3E
+
STZ $1B96
LDA $9D
BEQ +
RTS
+
LDA $1697
CMP #$07
BNE +
LDA #$06
STA $1697
+

LDA $15
CMP #$10
BCS .NoRNGManipulation1
JSL $81ACF9

.NoRNGManipulation1
LDA $15
CMP #$40
BNE .NoRNGManipulation2
JSL $81ACF9

.NoRNGManipulation2
LDA $15
CMP #$80
BNE .NoRNGManipulation3
JSL $81ACF9

.NoRNGManipulation3

.NoRNGManipulation4

	LDA $18C5
	BEQ .noWaveStuffJump
	LDX #$00
	BRA .waveLoop

.noWaveStuffJump
JMP .noWaveStuff
.waveLoop    

	LDA $18C5
	REP #$20
	AND #$00FF
	STA $03
	ASL
	ASL
	ASL
	STA $01
	LDA $03
	SEC
	SBC #$00FF
	SEP #$20
	STA $00
	JSR SIN
	STZ $0A		; $0A indicates if this number was negative or not.
	REP #$20
	LDA $03
	BPL +
	EOR #$FFFF
	INC A
	INC $0A
+
	LSR

	STA $00
	TXA
	AND #$00FF
	CLC
	ADC $18C5
	ASL #4
	CLC
	ADC #$0080
	STA $01
	SEP #$20
	JSR SIN

	REP #$20
	LDA $03	
	LDY $0A
	BNE .isNegative
	REP #$20
	CLC
	ADC #$2065
	BRA +
.isNegative
	LDA #$2065
	SEC
	SBC $03

+	STA $00


	PHX		; Preserve x; we need it to index a table.
	TXA
	ASL
	CLC
	ADC #$0040
	TAX
	LDA $00
	CMP #$2000
	BCS +
	LDA #$2000
+
	STA !FreeRamForMode2,x
	PLX
	INX
	CPX #$20
	BNE .waveLoop
	;LDA $14
	;AND #$01
	;BEQ +
	SEP #$30
	LDA $13D4
	BNE .noDecreaseTimer
	DEC $18C5
.noDecreaseTimer
STA $7FFFE0
	LDA $0F6B	; Don't affect the player if he's flying.
	BEQ +
	RTS
	+
	REP #$30
	LDA $D3
	SEC
	SBC #$008B
	STA $04		; $04 contains the player's y coord (feet-based) "relative" to the blimp's position
	
	LDA $D1
	LSR #3
	ASL
	TAX
	LDA !FreeRamForMode2+$40,x
	AND #$0FFF
	STA $00
	LDA !FreeRamForMode2+$42,x
	AND #$0FFF
	CMP $00
	BCS +
	LDA $00
	+

	SEC
	SBC #$0065	; We have to flip this over 65; tiles are stored upside down (a large y is higher up on the screen than a lower y)
	EOR #$FFFF
	INC A
	CLC
	ADC #$0065
	
	STA $00		; $00 holds the y position of the highest blimp "piece" that the player is touching / over.
	CMP $04
	SEP #$30
	BEQ .return
	BPL .belowPlayer
			; The highest piece is above the player.  Launch the player upwards.
	REP #$20
	LDA $00	
	CLC
	ADC #$008B
	STA $96
	LDA $00
	SEC
	SBC $04
	AND #$00FF
	SEP #$20
	ASL #4
	ORA #$80

	CMP $7D
	BPL +
	STA $7D
	+
	RTS
	
	
.belowPlayer
			; The highest piece is below the player.  Make the player fall.
	LDA #$01
	STA $185C
	
;+
.return
	RTS			; Skip the code where we level the playing field.


.noWaveStuff
	STZ $185C
	
	REP #$20	; \
	LDA $96		; |
	BMI +		; |
	CMP #$00F0	; |
	BCC +		; | Don't let the player fall off the screen.
	LDA #$00F0	; |
	STA $96		; |
	+		; /
	
	LDX #$40

.loop2    
	CPX #$80
	BEQ .done2
	LDA #$2065
	STA !FreeRamForMode2,x
	INX
	INX
	BRA .loop2
.done2


	
SEP #$20
LDA $77
AND #$04
BEQ .sprites

REP #$30
LDA $D1

LSR
LSR
DEC A
DEC A
DEC A
AND #$FFFE
TAX


LDA !FreeRamForMode2+$40,x
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
DEC A
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
SEC
SBC #$0004
STA !FreeRamForMode2+$40,x
INX 
INX

LDA !FreeRamForMode2+$40,x
SEC
SBC #$0004
STA !FreeRamForMode2+$40,x
INX 
INX

LDA !FreeRamForMode2+$40,x
DEC A
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
DEC A
STA !FreeRamForMode2+$40,x

;;INC A
;;STA !FreeRamForMode2+$40

.sprites
SEP #$30


LDY #$FF
.loop
INY
CPY #$0C
BEQ .noadjust

LDA $14C8,y
BEQ .loop
LDA $1588,y
AND #$04
BEQ .loop

LDA $14E0,y
XBA
LDA $00E4,y
REP #$30
;SEC
;SBC #$0008
LSR
LSR
DEC A
DEC A
DEC A
AND #$FFFE
TAX


LDA !FreeRamForMode2+$40,x
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
DEC A
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
SEC
SBC #$0004
STA !FreeRamForMode2+$40,x
INX 
INX

LDA !FreeRamForMode2+$40,x
SEC
SBC #$0004
STA !FreeRamForMode2+$40,x
INX 
INX

LDA !FreeRamForMode2+$40,x
DEC A
DEC A
STA !FreeRamForMode2+$40,x
INX
INX

LDA !FreeRamForMode2+$40,x
DEC A
STA !FreeRamForMode2+$40,x
SEP #$30
BRA .loop




.noadjust
	SEP #$10
	REP #$20
	LDX #$40

.loop3 
	CPX #$80
	BEQ .done3
	LDA !FreeRamForMode2,x
	CMP #$2061
	BCS $07
	LDA #$2061
	STA !FreeRamForMode2,x
	INX #2
	BRA .loop3
.done3

SEP #$30

	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;JSL.asm
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;$00 = radius
;$01 - $02 = angle
;$03 - $04 = result


SIN:		PHP
		PHX

		REP #$30		;16bit
		LDA $01
		AND #$00FF
		ASL A
		TAX				
		LDA $07F7DB,x
		STA $03			

		SEP #$30		;8bit	
		LDA $02			;$02
		PHA
		LDA $03			;|sin|
		STA $4202		;
		LDA $00			;
		LDX $04			;|sin| = 1.00
		BNE IF1_SIN
		STA $4203		;
		ASL $4216		;
		LDA $4217		
		ADC #$00
IF1_SIN:	LSR $02			;
		BCC IF_SIN_PLUS

		EOR #$FF
		INC A
		STA $03
		BEQ IF0_SIN
		LDA #$FF
		STA $04
		BRA END_SIN

IF_SIN_PLUS:	STA $03
IF0_SIN:	STZ $04

END_SIN:	PLA
		STA $02			;$02
		PLX
		PLP
		RTS

level75:
	LDA $1887	;	Quake code
	CMP #$30 
	BNE iwonderwhatthisdoes75
	LDA #$FF
	STA $1887

iwonderwhatthisdoes75:
	LDA $14		;\	Frequency of sound effect.
	AND #$01	; |
	BNE nope75	; | 
	LDA #$25	; |	Actual sound effect that is playing.
	STA $1DF9	;/

nope75:	LDA $24		;\	Stopping the tide at its max height.
	CMP #$DF	; |
	BEQ what75	;/	Unoptimized?

	LDA $9D		;\	9D has something to do with powerups.
	CMP #$00	; |	
	BNE what75	;/	If not 0, RTS

	LDA $14		;\	
	AND #$1F	; |	Every xth frame, Layer 3 rises one pixel.
	BNE what75	;/
	REP #$20	;\	Layer 3 rising code.
	INC $24		; |
	SEP #$20	;/
	what75:
	RTS

levelC7:
!c7ram1 = $0F60	;layer 1 y timer
!c7ram2 = $61	;wait timer 1
!c7ram3 = $62	;flash timer
!c7ram4 = $63	;hdma
!c7ram5 = $0F5E	;pause timer after flash
!c7ram6 = $0F5F	;flash 2 timer
!c7ram7 = $60	;hand over controls
!c7ram8 = $0F61	;brightness timer
!c7ram9 = $0F62	;pause 3
!c7ramA = $0F64 ;flash 3
!c7ramB = $0F65 ;wait fadein

	LDA $0100
	CMP #$07	;check if no menu is open
	BNE .JmpEnd

LDA $13
BIT #$1F
BNE .JmpEnd
 
LSR #4
AND #$02
TAX
REP #$30
LDA .Animations,x
TAY

LDA $7F837B
TAX
 
SEP #$20

.Loop
LDA $0000,y
STA $7F837D,x
INX
INY
INC A
BNE .Loop
 
SEP #$10
 
.JmpEnd
JMP .End
 
.Animations
dw .Anim1
dw .Anim2
 
.Anim1
incbin b1.stim
.Anim2
incbin b2.stim
 
.End
	STZ $7B		;freeze mario
	STZ $7D		;

	LDA !c7ram1	;check if layer 1 y is done
	BNE C7clear1

	LDA !c7ram5	;check if flash is over
	CMP #84
	BEQ C7pause1

	BRA C7bright

	LDA $0DAE
	CMP #$00
	BEQ C7bright
	DEC
	STA $0DAE
	RTS

C7bright:
	LDA $191F	;wait x frames before doing ANYTHING
	BEQ C7Go
	DEC
	STA $191F
	BRA C7Ret

C7Go:
	LDA $13
	AND #$03	;Run code slowly
	BEQ C7Ret

	INC $1464	;increase layer 1 y
	LDA $1464
	CMP #$E0	;if layer 1 y is done, tell me and return
	BNE C7Ret
	INC !c7ram1
	RTS



C7clear1:
	LDA !c7ram2
	CMP #23
	BEQ C7pauseend1	;wait after it reached position
	INC
	STA !c7ram2
	RTS


C7pauseend1:
	LDA !c7ram3
	CMP #10		;check how long white has been active/if it's over
	BEQ C7noflash1

	INC !c7ram3

;whiteflash
;	STZ $0D9F	;disable hdma

	REP #$20
	LDA #$77BD	;change background colour to white
	STA $0701
	SEP #$20

	LDA #$00	;hide layer 1 and sprite
	STA $212C
	STA $212D
	RTS

C7Ret:
	RTS

C7noflash1:
	LDA #$11	;re-enable layer 1 and sprite
	STA $212C
	STA $212D

	LDA #$18
	STA $1DFC

	REP #$20
	LDA #$0000	;change background colour back to black
	STA $0701
	SEP #$20

	LDA #$10
	STA $210D


C7pause1:
	LDA !c7ram5
	CMP #84
	BEQ C7pauseend2
	INC
	STA !c7ram5
	RTS


C7pauseend2:
	LDA !c7ram6
	CMP #10		;check how long white2 has been active/if it's over
	BEQ C7noflash2

	INC !c7ram6

	REP #$20
	LDA #$77BD	;change background colour to white
	STA $0701
	SEP #$20

	LDA #$00	;hide layer 1 and sprite
	STA $212C
	STA $212D

	LDA #00
	STA $1DFC
	
	RTS

C7noflash2:
	LDA !c7ram9
	CMP #42
	BEQ C7pauseend3
	INC
	STA !c7ram9

	LDA #$13	;show all layers but 3
	STA $212C
	STA $212D

	BRA C7hdma

C7pauseend3:
	LDA !c7ramA
	CMP #10		;check how long white3 has been active/if it's over
	BEQ C7noflash3

	INC !c7ramA

	STZ $0D9F	;disable hdma

	REP #$20
	LDA #$77BD	;change background colour to white
	STA $0701
	SEP #$20

	LDA #$00	;hide layer 1 and sprite
	STA $212C
	STA $212D

	LDA #00
	STA $1DFC
	
	RTS

C7noflash3:
	JSR C7hdma

	LDA #$FF
	STZ $0D9F	;enable hdma

	LDA #$18	;thunder
	STA $1DFC

	LDA #$1F	;show layer 3 menu and all the layers
	STA $212C
	STA $212D

	lda !c7ram7
	bne +
	LDA #$01	;activate l3 menu possibility
	STA !c7ram7
	+

	REP #$20
	LDA #$6770	;change background colour to white
	STA $0701
	SEP #$20



C7hdma:
		LDA #$00
		STA $4330
		LDA #$02
		STA $4340

		LDA #$32
		STA $4331
		STA $4341

		REP #$20
		LDA.w #.Title2
		STA $4332
		LDA.w #.Title1
		STA $4342
		SEP #$20

		LDA.b #.Title2>>16
		STA $4334
		LDA.b #.Title1>>16
		STA $4344

		LDA #$18
		TSB $0D9F

		RTS

.Title1
db $60,$26,$45
db $03,$27,$45
db $03,$28,$45
db $03,$29,$45
db $03,$2A,$45
db $03,$2B,$45
db $03,$2C,$45
db $02,$2D,$45
db $01,$2D,$44
db $03,$2E,$44
db $03,$2F,$44
db $03,$30,$44
db $04,$31,$44
db $03,$32,$44
db $03,$33,$44
db $03,$34,$44
db $03,$35,$43
db $03,$36,$43
db $04,$37,$43
db $03,$38,$43
db $03,$39,$43
db $03,$3A,$43
db $03,$3B,$43
db $02,$3C,$43
db $02,$3C,$42
db $03,$3D,$42
db $03,$3E,$42
db $35,$3F,$42
db $00

.Title2
db $68,$87
db $10,$86
db $11,$85
db $13,$84
db $44,$83
db $00

.c7mov:
db $05,$05,$05,$05,$05,$05,$05,$09,$FF
RTS

levelDD: 
	!Rotation = $0DC3
	stz $149f
	REP #$20
	LDA #$FFC0
	STA $2A
	LDA #$FFC0
	STA $2C
	SEP #$20
	REP #$20
	LDA !Rotation
	STA $36
	SEP #$20
	RTS


level10C:
!norveg_thunder_countdown_timer = $0DC3 ;Word
!get_random_number = $81ACF9
    stz $149f
    lda $9D
    ora $13D4
    bne +
    rep #$20
    lda !norveg_thunder_countdown_timer
    dec
    sta !norveg_thunder_countdown_timer
    sep #$20
    bpl +
        ; Play thunder sound
        lda.b #$18
        sta $1DFC
        ; Cause thunder palette animation
        lda $7FC0F8
        ora #$01
        sta $7FC0F8
        jsl !get_random_number
        rep #$20
        lda $148D
        and.w #$00FF
        clc
        adc #$0040
        sta !norveg_thunder_countdown_timer
        sep #$20
    +

    rts

level123:
	REP #$20				;
	LDA $1A				; Layer 1 X position
	LSR					; divided by 2
	LSR					; 4
	STA $22				; equals the X position for Layer 3

	LDA $1C				; Layer 1 Y position
	LSR					; divided by 2
	LSR					; 4
	STA $24				; equals the Y position for Layer 3
	SEP #$20				;
	RTS

level7C:
level74:
	REP #$20				;
	LDA $1A				; Layer 1 X position
	LSR					; divided by 2
	LSR					; 4
	STA $22				; equals the X position for Layer 3

	LDA $1C				; Layer 1 Y position
	LSR					; divided by 2

	STA $24				; equals the Y position for Layer 3
	SEP #$20				;
	RTS


level144:
	JSR levelA ;for l3bg
	
	LDA $0F42
	BEQ Rtn105
	CMP #$01
	BEQ Warp_105
	DEC $0F42	
Rtn105:
	RTS
Warp_105:
	LDA #$06
	STA $71
	STZ $89
	STZ $88
	RTS
level14A:
	JSR level182

	STZ $149F
	STZ $1414 ;/ V-Scroll = none 
	LDA $13 
	AND #$01 
	BNE Retuuurn 

	LDA $14AF
	BNE Upsee
	LDA $1468
	SEC
	SBC #$01
	STA $1468
	LDA $1469
	SBC #$00
	STA $1469 

	LDA $0F42
	BEQ Rtn14A
	CMP #$01
	BEQ Warp_14A
	DEC $0F42
	RTS

Retuuurn: 
	RTS
Upsee:
	LDA $1468
	CLC
	ADC #$01
	STA $1468
	LDA $1469
	ADC #$00
	STA $1469
	RTS
Warp_14A:
	LDA #$06
	STA $71
	STZ $89
	STZ $88
	RTS
Rtn14A:
	RTS

level182: 
	LDA $14AF
	STA $1487
	RTS
	
level1AA:
level1AB:
	LDA #$40
	STA $00
	STZ $01
	JMP sub_layer_3_scroll
	RTS


level10:
	LDA $D2
	CMP #$03
	BEQ .screen3
	CMP #$07
	BCS .screen7ormore
	RTS
.screen3		; Screen 3:
	LDA #$04	; disable rightwards scroll
	STA $5E		;
	RTS
.screen7ormore
	LDA $D2
	CMP #$07
	BNE .return
.screen7		; Screen 7:
	LDA $D1		; disable leftwards scroll
	CMP #$20	;
	BCC .noHscroll	;
	LDA #$01	;
	STA $1411	;
	RTS		;
.noHscroll		;
	STZ $1411	;
.return
	RTS



level11:
	JSR PoisonWater
	JSR Submerged

level13:
levelA1:
levelA2:
levelA3:
	LDA $77
	AND #$04
	BNE DontHitB

	LDA $0660
	BEQ DontHitB

	LDA $7D
	BPL DontHitB

	LDA #$80
	TSB $15
	RTS

DontHitB:
	STZ $0660
	RTS



level16:
	!Flag = $7C
	!YoshiHeadTile = $68	; The tile of Yoshi's head or whatever should be displayed
				; on the bottom-left corner of the screen.
	!Address = $0EFA
	!DisableRAM = $79
	PHP
	LDA !Flag
	BEQ .OffYoshi	
	;LDX $19
	;LDA StatusBarTiles,x
	;STA !Address

	; Disable cape hover/flying
	lda #$04
	sta !DisableRAM

	REP #$30
	LDA #$0000
	SEP #$20
	LDX #$0020
	LDY #$0000
	JSR .FindOAM
	LDA #$7F
	STA $78	
	LDA $19
	BEQ .NoDraw
	CMP #$01
	BEQ .DrawExtra
	LDA #$10
	STA $0200,x
	LDA #$D0
	STA $0201,x
	LDA #$24
	STA $0202,x
	LDA #%00111000
	STA $0203,x
	TXA
	LSR #2
	TAY
	LDA #$02
	STA $0420,y
.DrawExtra	
	JSR .FindOAM
	LDA #$00
	STA $0200,x
	LDA #$D0
	STA $0201,x
	LDA #$24
	STA $0202,x
	LDA #%00111000
	STA $0203,x
	TXA
	LSR #2
	TAY
	LDA #$02
	STA $0420,y
.NoDraw
	LDA #$80
	TSB $0DAC
.OffYoshi
	PLP
	RTS

.FindOAM
	slot_loop:
		LDA $0201,X
		CMP #$F0
		BEQ +
			INX #4
			CPX #$0200
			BCC slot_loop	; not sure why this was bcs
	+
	RTS




level17:
	STZ $149F
	RTS



level1A:
;HDMA
        LDA #$00
	STA $4330
	LDA #$02
	STA $4340

	LDA #$32
	STA $4331
	STA $4341

	REP #$20
	LDA.w #.Table2
	STA $4332
	LDA.w #.Table1
	STA $4342

	SEP #$20

	LDA.b #.Table2>>16
	STA $4334
	LDA.b #.Table1>>16
	STA $4344

	LDA #$18
	TSB $0D9F

		
; Enable Vertical Scroll on certain screens
	STZ $1412
	LDA $D2
	TAY
	LDA .vscroll,y
	BEQ .noscroll
	INC $1412
	RTS
.noscroll
	LDA $1C
	CMP #$C0
	BCS .noscroll_2
	INC $1412
.noscroll_2
	RTS
.vscroll  db $00,$00,$00,$00,$00,$01,$01,$01
	  db $00,$00,$00,$00,$00,$00,$01,$01
		 db $00,$00,$00,$01,$01,$00,$00,$00
		  db $00,$01,$00,$01,$01,$00,$00,$00
.Table1
	db $42,$21,$89
	db $0C,$21,$88
	db $02,$20,$88
	db $01,$21,$88
	db $01,$20,$88
	db $04,$21,$87
	db $02,$20,$87
	db $01,$21,$87
	db $07,$20,$87
	db $0D,$20,$86
	db $0F,$20,$85
	db $12,$20,$84
	db $52,$20,$83
	db $00

.Table2
	db $3C,$46
	db $01,$45
	db $01,$46
	db $19,$45
	db $15,$44
	db $18,$43
	db $5C,$42
	db $00


level59:
level1B:
	JSR level1C ;For Wave HDMA
	LDA $77
	AND #$04
	BNE Returnll1
	LDA #$80
	TRB $15
	TRB $16
	TRB $17
	TRB $18
	LDA #$08
	TRB $15
	LDA $14
	AND #$01
	BNE Returnll1
	LDA #$08
	TSB $15
Returnll1:
	RTS


level1F:
	LDA $1464 
	CLC
	ADC #$1D
	STA $24

	LDA $9D
	BNE .noscroll
	LDA $13D4
	BNE .return

.nevermind
	LDA $13
	AND #$03
	BEQ .return
.noscroll
	REP #$20
	DEC $22
	SEP #$20
.return
	RTS

level22:
	STZ $1473
	RTS

level2D:
	LDX #$09
.Loop
	LDA $9E,x
	CMP #$B2	; If falling spike
	BNE .LoopCall

	LDA $15F6,x
	ORA #$02
	STA $15F6,x	; Use palette 9 instead of palette 8.

.LoopCall
	DEX
	BPL .Loop

	REP #$20
	;LDA $1462
	;STA $22
	LDA $1464
	CLC
	ADC #$0020
	STA $24
	SEP #$20

	LDA $14AF
	BNE .NoCorLay3

	REP #$20
	LDA $1462
	STA $22
	SEP #$20

.NoCorLay3
	LDA $14AF
	BNE PalFade2D

	LDA $7FC086
	CMP #$08
	BCS Continue2D

	LDA #$00
	STA $7FC086
	STA $7FC085
	STA $7FC084
	BRA Continue2D

PalFade2D:
	LDA $7FC086
	CMP #$08
	BCC Continue2D

	LDA #$08
	STA $7FC086
	STA $7FC085
	STA $7FC084

Continue2D:
	RTS

PalFadeTBL2D:
	dw $49C8,$49C8,$3D87,$3545,$28E4,$20A2,$1461,$1461
	dw $1461,$1461,$20A2,$28E4,$3545,$3D87,$49C8,$49C8



!snowball_x_pos = $7FA002
!snowball_y_pos = $7FA004
!snowball_x_speed = $7FA006
!snowball_y_speed = $7FA007
!snowball_x_acc_bits = $7FA008
!snowball_y_acc_bits = $7FA009
!snowball_going_flag = $7FA00A
!snowball_timer = $7FA00B

level1E6:
    ;RTS ;test
	LDA.w $13D4
	BEQ +
	JMP .skip
+
	LDA $9D
	BEQ +
	JMP .Main2E_2
+
	LDA $1463
	CMP #$10
	BCS .SwitchIsOFF

	STZ $14AF
	BRA .NextSnowballStuff

.SwitchIsOFF
	LDA #$01
	STA $14AF

.NextSnowballStuff
	REP #$20
	LDA $7FA00B
	CMP #$0480
	SEP #$20
	BCS .LakituLoop1

	LDA $7FA00A	; Snowball event flag.

	BEQ .SetSnowballEvent

	LDA #$21
	STA $1DF9	; Make rumbling noise.

	REP #$20
	LDA $7FA00B
	INC A
	STA $7FA00B
	LSR A
	LSR A
	LSR A
	TAX
	SEP #$20

	PHX
	LDX #$04
.ShakeFramesLoop
	REP #$20
	LDA $7FA00B
	CMP .SnowballShakeFrames,x
	SEP #$20
	BEQ .ShakeTheGround
	DEX
	DEX
	BPL .ShakeFramesLoop
	BRA .SkipShakeStuff

.ShakeTheGround
	LDA #$20
	STA $1887	; Shake ground
	LDA #$09
	STA $1DFC

.SkipShakeStuff
	PLX
	LDA level1E6_SnowballXSpeedTBL,x
	STA $7FA006
	LDA level1E6_SnowballYSpeedTBL,x
	STA $7FA007
	JSR .ShatterBlocks

	BRA .LakituLoop

.SetSnowballEvent
	LDA $1463
	CMP #$12
	BNE .LakituLoop1

	LDA #$01
	STA $7FA00A

	REP #$20
	LDA #$EF80
	STA $7FA002
	LDA #$FF30
	STA $7FA004
	SEP #$20
	BRA .LakituLoop2

.LakituLoop1
	LDA #$00
	STA $7FA00A	;Unset flag
.LakituLoop2

.LakituLoop


.SilverPKill


.NotLakitu


	LDA $1463
	CMP #$03
	BCS level1E6_IceSkateStuff
	JMP level1E6_Main2E_2

.IceSkateStuff
LDA $77
AND #$04
BEQ .inair
LDA $7FA001
BEQ +
LDA $7FA000
STA $76
LDA #$00
STA $7FA001
BRA +
+
LDA $76
STA $7FA000
BRA +
.inair
LDA #$01
STA $7FA001
+

	REP #$20

	LDA $1462
	CLC
	ADC $7FA002
	STA $22

	LDA $22
	CMP #$0200
	BCC level1E6_BackToIceSkateness1

	STZ $22

.BackToIceSkateness1

	LDA $1464
	CLC
	ADC $7FA004
	STA $24
	BPL .Layer3YCheck

	LDA $24
	CMP #$FF00
	BCS .BackToIceSkateness

	LDA #$FF00
	STA $24
	BRA .BackToIceSkateness

.Layer3YCheck
	LDA $24
	CMP #$0100
	BCC .BackToIceSkateness

	LDA #$0100
	STA $24

.BackToIceSkateness

	SEP #$20

	LDA $7D
	BPL level1E6_IceSkateSpeed

	LDA #$01
	STA $140D	; Set spin-jump flag if jumping in the air.

.IceSkateSpeed
	LDA $15E8
	BEQ level1E6_CheckMarioDirection
	BMI level1E6_CheckLeft

	LDA $15E8
	CMP #$3E
	BCS level1E6_IceSkateFixSpeedRight
	BRA level1E6_CheckMarioDirection

.CheckLeft
	LDA $15E8
	CMP #$C3
	BCS level1E6_CheckMarioDirection

	LDA #$C3
	STA $15E8
	BRA level1E6_ConvertFreeRAMtoSpeed

.IceSkateFixSpeedRight
	LDA #$3D
	STA $15E8
	BRA level1E6_ConvertFreeRAMtoSpeed

.CheckMarioDirection
	LDA $76
	BNE level1E6_IncreaseSkateSpeed

	DEC $15E8
	DEC $15E8
	BRA level1E6_ConvertFreeRAMtoSpeed

.IncreaseSkateSpeed
	INC $15E8
	INC $15E8

.ConvertFreeRAMtoSpeed
	LDA $60
	BNE .Main2E	; Don't alter Mario's speed if on 32x32 ice block

	LDA $15E8
	STA $7B

.Main2E
	JSR .Layer3Speeds

	LDA $77
	AND #$03
	BEQ level1E6_Main2E_2

	LDA $7B
	BPL level1E6_PushBack8

	REP #$20
	LDA $94
	CLC
	ADC #$0008
	STA $94
	SEP #$20
	BRA level1E6_ReverseSkateSpeed

.PushBack8
	REP #$20
	LDA $94
	SEC
	SBC #$0008
	STA $94
	SEP #$20

.ReverseSkateSpeed
	LDA #$01
	STA $1DF9

	LDA $15E8
	BPL level1E6_HalfSpeedRight

	LDA $15E8
	EOR #$FF
	INC A
	LSR A
	STA $15E8	; Make Mario ricochet off walls.
	BRA level1E6_Main2E_2

.HalfSpeedRight
	LDA $15E8
	LSR A
	EOR #$FF
	INC A
	STA $15E8	; Make Mario ricochet off walls.

.Main2E_2
    ;RTS ;test
	REP #%00100000
	LDA.l $7FB320  	;;was $7FB0--
	CLC : ADC.w #$0001
	STA.l $7FB320 	;;was $7FB0--
	LDA.l $7FB322	;;was $7FB0--
	SEC : SBC.w #$0001
	STA.l $7FB322	;;was $7FB0--
	SEP #%00100000
.skip
	REP #%00010000
	LDA.b #%01000010
	STA.w $4360
	LDA.b #$0F
	STA.w $4361
	REP #%00100000
	LDA.b $20
	AND.w #$01FF
	STA.b $00
	ASL
	CLC
	ADC.b $00
	CLC
	ADC.w #.HTbl2
	STA.w $4362
	SEP #%00100000
	LDA.b #.HTbl2>>16
	STA.w $4364
	LDA.b #$7F
	STA.w $4367
	LDA.b #%1000000
	TSB.w $0D9F
	SEP #%00010000
	JSR .Grp1
	RTS

.Grp1
		LDA #$01
		STA $1413

		LDA $1C
		CMP #$B0
		BCC level1E6_RemoveScanline2E
		LDA $1C
		EOR #$FF
		CLC
		ADC #$11
		STA $7FB603
		BRA level1E6_SkippySkipSkip2E

.RemoveScanline2E
		LDA #$60
		STA $7FB603

.SkippySkipSkip2E
        ;RTS ; Skip HDMA for now.

		LDA #$00
		STA $4330
		LDA #$02
		STA $4340

		LDA #$32
		STA $4331
		STA $4341

		REP #$20
		LDA.w #.Table2
		STA $4332
		LDA.w #.Table1
		STA $4342

		SEP #$20

		LDA.b #.Table2>>16
		STA $4334
		LDA.b #.Table1>>16
		STA $4344

		LDA #$18
		TSB $0D9F

	REP #%00100000
	LDA.b $1E
	CLC : ADC.l $7FB320	;;was $7FB0--
	STA.b $00
	STA.b $00
	LDX.b $00
	STX.w $211B
	LDX.b $01
	STX.w $211B
	LDX.b #$20
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB308 ;;was $7FB0--
	LDX.b #$30
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB306 ;;was $7FB0--
	LDX.b #$40
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB304 ;;was $7FB0--
	LDX.b #$50
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB302 ;;was $7FB0--
	LDX.b #$60
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB300 ;;was $7FB0--
	LDX.b #$70
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB316	;;was $7FB0--
	LDA.b $00
	STA.l $7FB31A	;;was $7FB0--

	LDA.b $1E
	STA.b $00
	LDX.b $00
	STX.w $211B
	LDX.b $01
	STX.w $211B
	LDX.b #$10
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB30A	;;was $7FB0--
	LDX.b #$20
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB30C	;;was $7FB0--
	LDX.b #$30
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB30E	;;was $7FB0--
	LDX.b #$40
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB310	;;was $7FB0--
	LDX.b #$50
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB312	;;was $7FB0--
	LDA.b $00
	STA.l $7FB31E	;;was $7FB0--

	LDA.b $1E
	CLC : ADC.l $7FB322	;;was $7FB0--
	STA.b $00
	STA.b $00
	LDX.b $00
	STX.w $211B
	LDX.b $01
	STX.w $211B
	LDX.b #$60
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB314 	;;was $7FB0--
	LDX.b #$70
	STX.w $211C
	LDA.w $2134
	ASL
	AND.w #$FF00
	ADC.w #$0000
	XBA
	STA.l $7FB318	;;was $7FB0--
	LDA.b $00
	STA.l $7FB31C	;;was $7FB0--

	SEP #%00100000
	RTS

.SnowballShakeFrames
dw $0010,$0038,$0060

.SnowballXSpeedTBL
db $A0,$A0,$A0,$A0,$A0,$A0,$A0,$A0,$A0,$B0,$C0,$D0,$D0,$D0,$D0,$D0
db $D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0
db $D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0
db $D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0
db $D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0
db $D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0
db $D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0
db $D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0,$D0
db $D0,$D0,$D0,$D0,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

.SnowballYSpeedTBL
db $E0,$D0,$20,$10,$00,$F0,$E0,$20,$10,$00,$F0,$E0,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db $F0,$E0,$D0,$C0,$B0,$A0,$90,$80,$80,$80,$80,$80,$80,$80,$80,$80
db $80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80,$80

.Table1
db $07,$23,$42
db $08,$24,$43
db $08,$25,$44
db $08,$26,$44
db $08,$27,$45
db $08,$28,$46
db $08,$29,$46
db $08,$2A,$47
db $08,$2B,$48
db $08,$2B,$49
db $08,$2C,$49
db $08,$2D,$4A
db $08,$2E,$4B
db $08,$2F,$4B
db $08,$30,$4C
db $00

.Table2
db $07,$86
db $10,$87
db $08,$88
db $10,$89
db $08,$8A
db $10,$8B
db $08,$8C
db $10,$8D
db $10,$8E
db $08,$8F
db $00

.HTbl2	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B300
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B302
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B304
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B306
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B308
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30A
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30C
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B30E
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B310
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B312
	db $01 : dw $B300
	db $01 : dw $B314
	db $01 : dw $B300
	db $01 : dw $B314
	db $01 : dw $B300
	db $01 : dw $B314
	db $01 : dw $B300
	db $01 : dw $B314
	db $01 : dw $B300
	db $01 : dw $B314
	db $01 : dw $B300
	db $01 : dw $B314
	db $01 : dw $B300
	db $01 : dw $B314
	db $01 : dw $B300
	db $01 : dw $B314
	db $01 : dw $B316
	db $01 : dw $B316
	db $01 : dw $B318
	db $01 : dw $B318
	db $01 : dw $B316
	db $01 : dw $B316
	db $01 : dw $B318
	db $01 : dw $B318
	db $01 : dw $B316
	db $01 : dw $B316
	db $01 : dw $B318
	db $01 : dw $B318
	db $01 : dw $B316
	db $01 : dw $B316
	db $01 : dw $B318
	db $01 : dw $B318
	db $01 : dw $B31A
	db $01 : dw $B31A
	db $01 : dw $B31A
	db $01 : dw $B31A
	db $01 : dw $B31C
	db $01 : dw $B31C
	db $01 : dw $B31C
	db $01 : dw $B31C
	db $01 : dw $B31A
	db $01 : dw $B31A
	db $01 : dw $B31A
	db $01 : dw $B31A
	db $01 : dw $B31C
	db $01 : dw $B31C
	db $01 : dw $B31C
	db $01 : dw $B31C
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $01 : dw $B31E
	db $00

.Layer3Speeds
	LDX #$01
.CameraMAIN
	LDA $7FA006,x	; Camera X speed.
	BMI .CameraX_Left

.Camera_Right
	LDA $7FA008,x	; Amount of "subpixels" to add to the camera's X position.
	CMP #$10
	BCC .CameraSubPix_Add

	LDA $7FA008,x
	SEC
	SBC #$10
	STA $7FA008,x

	TXA
	ASL A
	TAX

	REP #$20
	LDA $7FA002,x
	INC A
	STA $7FA002,x	; Increase camera X position.
	SEP #$20

	TXA
	LSR A
	TAX
	BRA .Camera_Right

.CameraSubPix_Add
	LDA $7FA008,x	; Amount of "subpixels" to add to the camera's X position.
	CLC
	ADC $7FA006,x
	STA $7FA008,x
	DEX
	BPL .CameraMAIN
	RTS

.CameraX_Left
	LDA $7FA008,x	; Amount of "subpixels" to add to the camera's X position.
	CMP #$10
	BCC .CameraSubPix_Minus

	LDA $7FA008,x
	SEC
	SBC #$10
	STA $7FA008,x

	TXA
	ASL A
	TAX

	REP #$20
	LDA $7FA002,x
	DEC A
	STA $7FA002,x	; Decrease camera X position.
	SEP #$20
	TXA
	LSR A
	TAX
	BRA .CameraX_Left

.CameraSubPix_Minus
	LDA $7FA008,x	; Amount of "subpixels" to add to the camera's X position.
	SEC
	SBC $7FA006,x
	DEC A
	STA $7FA008,x
	DEX
	BPL .CameraMAIN
	RTS

.ShatterBlocks
	LDA $14
	AND #$03
	ASL A
	TAX
.ShatterBlocksLoop
	REP #$20
	LDA $98
	PHA
	LDA $9A
	PHA

	LDA $7FA002
	EOR #$FFFF
	CLC
	ADC #$0140
	STA $9A
	LDA .ShatterYOffset,x
	STA $98

	SEP #$20
	PHY		;preserve map16 high

	;LDA $14
	;AND #$0F
	;BNE .SkipShatter

	PHB		;need to change bank
	LDA #$02
	PHA
	PLB		;to 02
	LDA #$00	;default shatter
	JSL $828663	;shatter block
	PLB		;restore bank

.SkipShatter
	LDA #$02
	STA $9C
	JSL $80BEB0
	PLY

	REP #$20
	PLA
	STA $9A
	PLA
	STA $98
	SEP #$20

	;DEX
	;DEX
	;BPL level2E_ShatterBlocksLoop
	RTS

.ShatterYOffset
dw $0160,$0170,$0180,$0190

level3A:
LDX #$0B
-
LDA $9E,x	; if sprite number isn't
CMP #$C4	; C4
BNE ++		; skip code
LDA $C2,x	; if an unused state
BEQ +		; is zero, branch
STZ $AA,x	; kill speed
JSR controls
BRA ++
+
LDA $1540,x	;if falling timer isn't set
BEQ ++		; skip code
STZ $1540,x	; destroy timer
INC $C2,x	; increase a state unused by the sprite
STZ $AA,x	; kill speed
++
DEX		; decrease x index
BPL -		; branch until looping done
RTS
controls:

LDA $16
AND #$01
BNE KeepGoingUh
LDA $16
AND #$02
BEQ KeepGoingUh
LDA #$2A
STA $1DFC


KeepGoingUh:
LDA #$0
CMP $76
BCC NotPressingLeft
STZ $7B

NotPressingLeft:

LDA #$01
STA $1412


LDA #$00
CMP $73
BEQ controls2  ;if is ducking, run this

LDA $15
AND #$01
BNE left
LDA $15
AND #$02
BEQ controls2	

right:
DEC $94
DEC $7E
JMP controls2
left:
STZ $7B
controls2:

LDA $94 ; mario x low
SEC
SBC #$18 ; offset sprite to mario
STA $E4,x ; sprite x low
LDA $95 ; mario x high
SBC #$00 ; only change this to #$FF if the sprite offset is #$80 to #$FF
STA $14E0,x ; sprite x high



LDA $15
AND #$04
BNE down
LDA $15
AND #$08
BEQ notup


up:
LDA $D8,x
SEC
SBC #$02 ; always make it double the below ADC #$xx
STA $D8,x
LDA $14D4,x
SBC #$00
STA $14D4,x
down:
LDA $D8,x
CLC
ADC #$01
STA $D8,x
LDA $14D4,x
ADC #$00
STA $14D4,x
notup:
RTS



level46:
level47:
level48:
	JSR PoisonWater
	JSR Submerged
	JMP level14B





level5B:
	STZ $1413		; \
	STZ $1414		;  |
	REP #$20		;  |
	LDA $1462		;  |
	LSR #3			;  |
	STA $1466		;  | Custom BG scroll
	LDA $1464		;  |
	LSR #4			;  |
	CLC : ADC #$1468	;  |
	SEC : SBC #$000A	;  |
	STA $1468		;  |
	SEP #$20		; /

	LDA $97			; \
	BNE .run		;  |
	LDA $95			;  |
	BEQ .return		;  | forces Mario's X position
	.run			;  | to be between #$0080 and #$0180
	LDA $95			;  | (creating invisible barriers)
	BEQ .left		;  | execpt in the left half of screen 0
	LDA $94			;  |
	CMP #$80		;  |
	BCC .return		;  |
	LDA #$80 : STA $94	;  |
	BRA .return		;  |
	.left			;  |
	LDA $94			;  |
	CMP #$80		;  |
	BCS .return		;  |
	LDA #$80 : STA $94	;  |
.return				;  |
	RTS			; /





level70:
LDA #$00
		STA $4330
		LDA #$02
		STA $4340

		LDA #$32
		STA $4331
		STA $4341

		REP #$20
		LDA.w #.Table2
		STA $4332
		LDA.w #.Table1
		STA $4342

		SEP #$20

		LDA.b #.Table2>>16
		STA $4334
		LDA.b #.Table1>>16
		STA $4344

		LDA #$18
		TSB $0D9F



LDA $9D    ;\
BNE .return ;/ If sprites are locked, return.
LDA $13D4
BNE .return
;Scroll L3
LDA $14
LSR
AND #$01
BEQ .nol3scroll
REP #$20
INC $22
SEP #$20
.nol3scroll
;L2
STZ $1413  ;\ H-Scroll = none
STZ $1414  ;/ V-Scroll = none
REP #$20
LDA $1462
SEC
SBC $1466
CMP #$02A0
BCS .nochasefast
SEP #$20
JSR .chasefast
BRA .skip
.nochasefast
SEP #$20
LDA $14
LSR
AND #$3F
TAY
LDA $1466
SEC
SBC .speeds,y
STA $1466
LDA $1467
SBC .speedsh,y
STA $1467
.skip
;push the player
JSR .pushmario
;L1
STZ $1411
LDA #$03
STA $143E
JSR .incL1
LDA $14
AND #$03
BEQ .notwice
JSR .incL1
.notwice
RTS
.return
RTS
.incL1
REP #$20
LDA $1462
INC
STA $1462
SEP #$20
RTS
.pushmario
		LDA $7E
		CMP #$10
		BCC .pushyes
		RTS
.pushyes
		REP #$20
		LDA $94
		CLC
		ADC #$0002
		STA $94
		SEP #$20
		RTS
.chasefast
		LDA $14
		AND #$01
		BEQ .notchasefast
		REP #$20
		DEC $1466
		SEP #$20
.notchasefast
		;RTS
		LDA #$00
		STA $00
		STZ $01
		JMP sub_layer_3_scroll
.speeds  db $00,$00,$00,$FF,$00,$FF,$00,$00
		 db $00,$00,$01,$00,$00,$00,$00,$01
		 db $00,$00,$00,$FF,$00,$FF,$00,$00
		 db $00,$00,$01,$00,$00,$00,$00,$01
		 db $00,$00,$00,$FF,$00,$FF,$00,$00
		 db $00,$00,$01,$00,$00,$00,$00,$01
		 db $00,$00,$00,$FF,$00,$FF,$00,$00
		 db $00,$00,$01,$00,$00,$00,$00,$01
.speedsh db $00,$00,$00,$FF,$00,$FF,$00,$00
		 db $00,$00,$00,$00,$00,$00,$00,$00
		 db $00,$00,$00,$FF,$00,$FF,$00,$00
		 db $00,$00,$00,$00,$00,$00,$00,$00
		 db $00,$00,$00,$FF,$00,$FF,$00,$00
		 db $00,$00,$00,$00,$00,$00,$00,$00
		 db $00,$00,$00,$FF,$00,$FF,$00,$00
		 db $00,$00,$00,$00,$00,$00,$00,$00
.Table1
db $25,$21,$4A
db $02,$21,$49
db $01,$21,$4A
db $15,$21,$49
db $11,$21,$48
db $08,$21,$47
db $01,$20,$47
db $01,$21,$47
db $04,$20,$47
db $02,$21,$46
db $0C,$20,$46
db $10,$20,$45
db $14,$20,$44
db $52,$20,$43
db $00

.Table2
db $32,$8F
db $0D,$8E
db $0A,$8D
db $0A,$8C
db $09,$8B
db $09,$8A
db $0A,$89
db $0A,$88
db $0B,$87
db $0E,$86
db $4E,$85
db $00

level78:
	LDA $1887	;	Quake code
	CMP #$30 
	BNE iwonderwhatthisdoes78
	LDA #$FF
	STA $1887

iwonderwhatthisdoes78:
	LDA $14		;\	Frequency of sound effect.
	AND #$01	; |
	BNE nope78	; | 
	LDA #$25	; |	Actual sound effect that is playing.
	STA $1DF9	;/

nope78:	LDA $24		;\	Stopping the tide at its max height.
	CMP #$DF	; |
	BEQ what78	;/	Unoptimized?

	LDA $9D		;\	9D has something to do with powerups.
	CMP #$00	; |	
	BNE what78	;/	If not 0, RTS

loop78:	LDA $14		;\	
	AND #$0F	; |	Every xth frame, Layer 3 rises one pixel.
	BNE what78	;/
	REP #$20	;\	Layer 3 rising code.
	INC $24		; |
	SEP #$20	;/
	what78:
	RTS

level82:
	STZ $149F
	RTS

level136:
level83:
	LDX #$0B
Loopdidoo:
LDA $7FAB9E,x ; \ if the custom sprite number is
CMP #$62 ; | pionpi
BNE NextSprite ; /

LDA $166E,x
ORA #$30
STA $166E,x ; / Fire and cape immunity

NextSprite:
DEX ;
BPL Loopdidoo ; ...otherwise, loop
RTS ; or return

level85:
	LDA $19
	CMP #$02
	BNE Retoin
	LDA #$03
	STA $19
Retoin: 
        RTS
level86:
	LDA $18C5
	AND #$01	
	CMP #$01
	BEQ Floor2_86
	REP #$20
	LDA $D3
	CMP #$0100
	BNE Floor2_86
	LDA $D1	
	CMP #$04B0
	BCC Floor2_86
	CMP #$0510
	BCS Floor2_86
	LDA #$00A0
	STA $98
	LDA #$0520
	STA $9A
	SEP #$20
	LDA $18C5
	ORA #$01
	STA $18C5
	LDA #$01
	STA $14AF
	JSR Shatter101
	LDA #$02
	STA $9C
	JSL $80BEB0
	REP #$20
	LDA #$00C0
	STA $98
	LDA #$04C0
	STA $9A
	SEP #$20
	JSR Shatter101

	Floor2_86:
	SEP #$20
	LDA $18C5
	AND #$02	
	CMP #$02
	BEQ Return86
	REP #$20
	LDA $D3
	CMP #$0040
	BNE Return86
	LDA $D1
	CMP #$0100
	BCC Return86
	CMP #$0120
	BCS Return86

	LDA #$0060
	STA $98
	LDA #$0100
	STA $9A
	SEP #$20
	LDA $18C5
	ORA #$02
	STA $18C5
	LDA #$02
	STA $9C
	JSL $80BEB0
	REP #$20
	LDA #$0110
	STA $9A
	SEP #$20
	JSR Shatter101
	JSL $80BEB0
	REP #$20
	LDA #$0120
	STA $9A
	SEP #$20
	JSL $80BEB0
	Return86:
	SEP #$20
	RTS
level87:
	LDA $D2
	CMP #$07
	BNE End87
	LDA $18C5
	CMP #$01
	BEQ End87
	LDA #$01
	STA $18C5
	JSR Shatter101
	End87:
	RTS
level88:
	LDA $71
	CMP #$00
	BNE Return88
	LDA $13D4
	CMP #$00
	BEQ RunCode_88
	Return88:
	RTS

	RunCode_88:
	LDA $18C5
	CMP #$00
	BNE SkipTrigger88
	REP #$20
	LDA $D1
	CMP #$0A30
	BCC SkipTrigger88
	SEP #$20
	LDA #$01
	STA $18C5
	REP #$20
	LDA #$0A00
	STA $9A
	LDA #$0170
	STA $98
	SEP #$20
	Loop_88:
	JSR Shatter101
	LDA $9A
	CLC
	ADC #$50
	STA $9A
	CMP #$B0
	BCC Loop_88

	SkipTrigger88:
	SEP #$20
	LDA $18C5
	CMP #$02
	BEQ D2_88
	CMP #$03
	BEQ D3_88
	CMP #$04
	BEQ Mid88
	CMP #$00
	BEQ Mid88
	REP #$20
	LDA $1468
	CMP #$003E
	BCC SD2_88
	SEC
	SBC #$0003
	STA $1468
	STA $60
	SEP #$20
	JMP Mid88
	D2_88:
	REP #$20
	LDA $1468
	CMP #$0045
	BCS SD3_88
	INC $1468
	INC $1468
	STA $60
	SEP #$20
	JMP Mid88
	SD2_88:
	SEP #$20
	LDA #$09
	STA $1DFC
	LDA #$1A
	STA $1887
	LDA #$02
	STA $18C5
	JMP Mid88
	D3_88:
	REP #$20
	LDA $1468
	CMP #$003E
	BCC SD4_88
	DEC $1468
	STA $60
	SEP #$20
	JMP Mid88
	SD3_88:
	SEP #$20
	LDA #$03
	STA $18C5
	JMP Mid88
	SD4_88:
	SEP #$20
	LDA #$04
	STA $18C5
	Mid88:
	LDA $18C7
	CMP #$00
	BEQ Check2_88
	CMP #$08
	BCS Break1_88
	INC $18C7
	JMP Check2_88

	Break1_88:
	STZ $18C7
	REP #$20
	LDA $13E6
	STA $98
	LDA $1869
	STA $9A
	SEP #$20
	LDA #$02
	STA $9C
	JSR Shatter101
	JSL $80BEB0
	LDA $1864
	CMP #$02
	BNE D2_1_88
	LDA $9A
	CLC
	ADC #$10
	STA $9A
	JSL $80BEB0
	JMP Draw_88
	D2_1_88:
	CMP #$01
	LDA $9A
	SEC
	SBC #$10
	STA $9A
	JSL $80BEB0
	Draw_88:
	STZ $13E6
	STZ $13E7
	STZ $1869
	STZ $186A
	STZ $1864
	
	Check2_88:
	JMP sub_layer_3_scroll
		;RTS
level89:
	LDA $71
	CMP #$00
	BNE Return89
	LDA $13D4
	CMP #$00
	BEQ RunCode_89
	Return89:
	RTS
	RunCode_89:
	JSR Mid88
	RTS
level8A:
	LDA $71
	CMP #$00
	BNE ReturnReturn8A
	LDA $13D4
	CMP #$00
	BEQ RunCode_8A
	ReturnReturn8A:
	RTS
	RunCode_8A:
	JSR Mid88
	LDA $18C5
	CMP #$01
	BEQ Frame2_8A
	CMP #$02
	BEQ Frame3_8A
	CMP #$03
	BEQ Return8A
	LDA $D2
	CMP #$0A
	BNE Return8A
	REP #$20
	LDA $D3
	CMP #$0160
	BNE Return8A
	LDA #$0130
	STA $98
	LDA #$0A20
	STA $9A
	SEP #$20
	LDA #$02
	STA $9C
	LDA #$09
	STA $1DFC
	LDA #$14
	STA $1887
	Loop1_8A:
	JSL $80BEB0
	LDA $98
	CLC
	ADC #$10
	STA $98
	CMP #$40
	BNE ShatterSkip1_8A
	JSR Shatter101
	ShatterSkip1_8A:
	CMP #$60
	BNE Loop1_8A
	LDA #$01
	STA $18C5
	RTS
	Frame2_8A:
	REP #$20
	LDA #$00C0
	STA $98
	LDA #$0A30
	STA $9A
	SEP #$20
	LDA #$02
	STA $9C
	Loop2_8A:
	JSL $80BEB0
	LDA $9A
	CLC
	ADC #$10
	STA $9A
	CMP #$40
	BNE ShatterSkip2_8A
	JSR Shatter101
	ShatterSkip2_8A:
	CMP #$60
	BNE Loop2_8A
	LDA #$02
	STA $18C5
	RTS
	Return8A:
	SEP #$20
	RTS
	Frame3_8A:
	REP #$20
	LDA #$0070
	STA $98
	LDA #$0A30
	STA $9A
	SEP #$20
	LDA #$02
	STA $9C
	Loop3_8A:
	JSL $80BEB0
	LDA $9A
	CLC
	ADC #$10
	STA $9A
	CMP #$60
	BNE Loop3_8A
	LDA #$03
	STA $18C5
	RTS

level8C:
	REP #$20			;\ Set processor 16 bit
	LDA #$0F02			;| $4340 = $210F
	STA $4340			;| $4341 = Mode 02
	LDA #$9F00			;| 
	STA $4342			;| Destination: $7F9F00
	LDY #$7F			;| (low and high byte)
	STY $4344			;| 
	SEP #$20			;/ Set processor 8 bit

	LDA #$10			;\ Enable HDMA
	TSB $0D9F			;/ on channel 4

	LDX #$00			;\ Init of
	LDY #$00			;/ X and Y

	SEP #$20			;\ Set processor 8 bit
	LDA $13				;| Set speed of waves
	LSR A				;| Adding more LSR A
	LSR A				;| will make it slower
	STA $00				;/ Store in scratch RAM

	PHB					;\ Push data bank
	PHK					;| Push program bank
	PLB					;/ Pull data bank

Wave_Loop:
	LDA #$06			;\ Set scanline height
	STA $7F9F00,x		;| for each wave
	TYA					;| Transfer Y to A
	ADC $00				;| Add in scratch RAM
	AND #$0F			;| and transfer #$0F bytes
	PHY					;| Push Y
	TAY					;/ Transfer A to Y

	LDA.w Wave_Table,y	;\ Load in wave values
	LSR A				;| half of waves only
	CLC					;| Clear carry flag
	ADC $1466			;| Apply to layer 2
	STA $7F9F01,x		;| X position low byte
	LDA $1467			;| And add nothing to
	ADC #$00			;| layer 2 X position
	STA $7F9F02,x		;/ high byte

	PLY					;\ Pull Y
	CPY #$25			;| Compare with #$25 scanlines
	BPL End_Wave		;| If bigger, end HDMA
	INX					;| Increase X
	INX					;| Increase X
	INX					;| Increase X
	INY					;| Increase Y
	BRA Wave_Loop		;/ Do the loop

End_Wave:
	PLB					;\ Pull data bank
	LDA #$00			;| End HMDA by writing
	STA $7F9F03,x		;| #$00 here
	RTS					;/ Return

Wave_Table:
	db $00
	db $01
	db $02
	db $03
	db $04
	db $05
	db $06
	db $07
	db $07
	db $06
	db $05
	db $04
	db $03
	db $02
	db $01
	db $00
	RTS



levelA9:
REP #$20
LDA #$0000
STA $4330
LDA #CODE
STA $4332
PHK
PLY
STY $4334
SEP #$20
LDA #$08
TSB $0D9F
RTS

CODE:
db $03,$00
db $03,$01
db $03,$02
db $03,$03
db $03,$04
db $03,$05
db $03,$06
db $03,$07
db $03,$08
db $03,$09
db $03,$0A
db $03,$0B
db $03,$0C
db $03,$0D
db $03,$0E
db $80,$0F
db $03,$0E
db $03,$0D
db $03,$0C
db $03,$0B
db $03,$0A
db $03,$09
db $03,$08
db $03,$07
db $03,$06
db $03,$05
db $03,$04
db $03,$03
db $03,$02
db $03,$01
db $03,$00
db $00

levelAA:

LDA $75
BNE Return9
LDA $79
BNE CheckGround
LDA $7D
BPL Return9
LDA $7C
CMP #$03
BEQ Tits
INC $7C
BRA Return9
Tits:
STZ $7C
LDA $14AF
EOR #$01
STA $14AF
INC $79
BRA Return9
CheckGround:
LDA $77
AND #$04
BEQ Return9
STZ $79
Return9:
RTS

levelAC:
LDA $58
BEQ Return24
CMP #$01
BNE Yeah
LDA #$29
STA $1DFC      ;Ping Pong!
Yeah:
DEC $58
LDA $15
AND #$03
BEQ Check1
LDA $15
EOR #$03
STA $15
Check1:
LDA $16
AND #$03
BEQ Return24
LDA $16
EOR #$03
STA $16
Return24:
RTS


levelBB:

	LDA $1DEF	; freeram used for start of race timer
	CMP #$01
	BEQ StartBB
	CMP #$02
	BEQ StartSoundBB
	CMP #$03
	BEQ EndDrumBB
	CMP #$A0
	BEQ ReadySoundBB
	BRA DecTimeBB
StartSoundBB:
	LDA #$29
	STA $1DFC
	LDA #$40
	STA $7B
	BRA DecTimeBB
EndDrumBB:
	LDA #$12
	STA $1DFC
	BRA DecTimeBB
ReadySoundBB:
	LDA #$11
	STA $1DFC
DecTimeBB:
	DEC $1DEF
StartBB:

	LDA $13D4
	BNE DoneBB
	LDA $7B
	BMI DoneBB
	LDA $13C8
	BNE DecRAMBB
	LDA $16
	AND #$01
	BNE MoveBB
	BRA DoneBB
DecRAMBB:
	DEC $13C8
	LDA $16
	AND #$01
	BNE BoostBB
	BRA DoneBB
BoostBB:
	LDA $7B
	CLC
	ADC #$18
	CLC
	CMP #$7F
	BCC NoSlowBB
	LDA #$7F
NoSlowBB:
	STA $7B
MoveBB:
	LDA #$0A
	STA $13C8

DoneBB:
	RTS

levelBE:
	LDX #$0B
LoopBEtwo:
	LDA $9E,x		; \ if the sprite number is
	CMP #$80		;  | key
	BNE NextBEtwo		; /

	LDA $15F6,x
	ORA #$08
	STA $15F6,x	; / make it red (if it's gold or red)

NextBEtwo:	    
	DEX		;
	BPL LoopBEtwo	; ...otherwise, loop
	LDX #$0B
LoopBE:
	LDA $9E,x		; \ if the sprite number is
	CMP #$0F		;  | Goomba
	BNE NextBE		; /
	LDA $AA,x
	BMI NextBE			; \ ...and not one that is going into the sky
	LDA #$02
	STA $14C8,x	; / Kill it!

NextBE:	    
	DEX		;
	BPL LoopBE	; ...otherwise, loop
NoBreak:
	RTS		; return

levelE0:
levelE1:
	LDA $15
	AND #$03
	BEQ .DontInvertHorz
	LDA $15
	EOR #$03
	STA $15
	.DontInvertHorz
	LDA $15
	AND #$0C
	BEQ .DontInvertVert
	LDA $15
	EOR #$0C
	STA $15
	.DontInvertVert
	RTS

levelEE:
JSR level1C
LDA $80			;Mario's Ypos (screen)
CMP #208		;#$C3
BCC hopEE		;If equal or less, don't counterbreak moving under the level

LDA #$9D
BEQ hopEE
JSL $80F606


hopEE:
LDA $9D
BNE .noscrollEE
LDA $13D4
BNE .returnEE
.nevermindEE
LDA $14
AND #$03
BNE .returnEE
.noscrollEE
REP #$20
DEC $22
SEP #$20
.returnEE

returnEE:
REP #$20		; Extend Accumulator to 16-bits 
LDA $22 		; Load Layer 3's X Position 
CLC			; For Adding 
ADC $17BD 		; Add The Change that was made by the camera (to negate it) 
STA $22 		; Store Layer 3's X Position 
SEP #$20 		; Unextend Accumulator from 16-bits
RTS			; Return

levelE5: 
	JSR level1C ;For Wave HDMA

levelE9: 
levelEF:

LDA $C2,x	;\
BNE SkipThis	; |
		; |
LDA $24		; | This wouldnt work in the init so I had to do it the cheap way
ADC #$0B	; |
STA $C2,x	; | Dear code:
		; | </3
		;/  with love, GN
SkipThis:	

LDA $14AD
BEQ Doneef

LDA #$01
STA $1427

LDA $9D
CMP #$00
BNE SkipEF

LDA $1594,x
CMP #$40
BCS SkipEF

LDA $14
AND #$01
BNE SkipEF

INC $24
INC $1594,x
BRA SkipEF

Doneef:
LDA $1594,x
BEQ SkipEF
LDA $24
CMP $C2,x
BNE LowerLOW
STZ $1594,x
STZ $1427
BRA SkipEF

LowerLOW:
LDA $13
AND #$01
BNE SkipEF
DEC $24

SkipEF:

LDA $9D
BNE .noscrollEF
LDA $13D4
BNE .returnEF
.nevermindEF
LDA $13
AND #$03
BEQ .returnEF
.noscrollEF
REP #$20
DEC $22
SEP #$20
.returnEF

LDA #$FA
STA $1458
ReturnEF:
	RTS

levelFB:
	STZ $0DC1
	RTS


level101:
	LDA $18C5
	CMP #$01
	BEQ Return101
	REP #$20
	LDA $D3
	STA $60	
	CMP #$0160
	BNE Return101
	LDA $D1	
	CMP #$0160
	BCC Return101
	CMP #$0190
	BCC Tile101
	Return101:
	SEP #$20
	RTS
	Tile101:	
	LDA #$0180
	STA $98
	LDA #$0160
	STA $9A
	SEP #$20
	LDA #$01
	STA $18C5
	JSR Shatter101
	TileLoop101:
	LDA #$02
	STA $9C
	JSL $80BEB0
	LDA $9A
	CLC
	ADC #$10
	STA $9A
	CMP #$A0
	BNE TileLoop101
	JSR Shatter101
	RTS

	Shatter101:
	PHB
	LDA #$02
	PHA
	PLB
	LDA #$00
	JSL $828663 ;was JSR
	PLB
	RTS
level103:
level149:
	LDA $1462	;Load the layer 1 x position 
	STA $22		;Store to the layer 3 x position 
	LDA $1464	;Load the layer 1 y position
	STA $24		;Store to the layer 3 y position	

        LDA #$15    ;\
	STA $212D   ;/add some layer to the subscreen

	LDA #$00
	STA $4330
	LDA #$02
	STA $4340

	LDA #$32
	STA $4331
	STA $4341

	REP #$20
	LDA $20
	SEC
	SBC #$0000
	STA $02
	ASL A
	STA $00
	LDA.w #.Table2_103
	CLC
	ADC $00
	STA $4332

	LDA $02
	ASL A
	CLC
	ADC $02
	STA $00
	LDA.w #.Table1_103
	CLC
	ADC $00
	STA $4342

	SEP #$20

	LDA.b #.Table2_103>>16
	STA $4334
	LDA.b #.Table1_103>>16
	STA $4344

	LDA #$18
	TSB $0D9F
	RTS

.Table1_103
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$25,$44
db $01,$26,$44
db $01,$26,$44
db $01,$26,$45
db $01,$26,$45
db $01,$27,$45
db $01,$27,$45
db $01,$27,$45
db $01,$27,$45
db $01,$28,$45
db $01,$28,$46
db $01,$28,$46
db $01,$28,$46
db $01,$29,$46
db $01,$29,$46
db $01,$29,$47
db $01,$2A,$47
db $01,$2A,$47
db $01,$2A,$47
db $01,$2B,$48
db $01,$2B,$48
db $01,$2B,$48
db $01,$2C,$48
db $01,$2C,$48
db $01,$2C,$49
db $01,$2D,$49
db $01,$2D,$49
db $01,$2D,$49
db $01,$2E,$4A
db $01,$2E,$4A
db $01,$2F,$4A
db $01,$2F,$4A
db $01,$2F,$4B
db $01,$30,$4B
db $01,$30,$4B
db $01,$30,$4B
db $01,$31,$4C
db $01,$31,$4C
db $01,$32,$4C
db $01,$32,$4C
db $01,$32,$4D
db $01,$33,$4D
db $01,$33,$4D
db $01,$34,$4D
db $01,$34,$4E
db $01,$34,$4E
db $01,$35,$4E
db $01,$35,$4E
db $01,$35,$4F
db $01,$36,$4F
db $01,$36,$4F
db $01,$37,$4F
db $01,$37,$50
db $01,$37,$50
db $01,$38,$50
db $01,$38,$50
db $01,$38,$51
db $01,$39,$51
db $01,$39,$51
db $01,$39,$51
db $01,$3A,$51
db $01,$3A,$52
db $01,$3A,$52
db $01,$3B,$52
db $01,$3B,$52
db $01,$3B,$52
db $01,$3C,$53
db $01,$3C,$53
db $01,$3C,$53
db $01,$3D,$53
db $01,$3D,$53
db $01,$3D,$54
db $01,$3D,$54
db $01,$3D,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $00

.Table2_103
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $01,$93
db $00

levelFC:
level112:
	LDA #$01
	STA $1404
	RTS

level117:
	LDA #$00
	STA $4330
	LDA #$02
	STA $4340

	LDA #$32
	STA $4331
	STA $4341

	REP #$20
	LDA $20
	SEC
	SBC #$0060
	STA $02
	ASL A
	STA $00
	LDA.w #.Table2_117
	CLC
	ADC $00
	STA $4332

	LDA $02
	ASL A
	CLC
	ADC $02
	STA $00
	LDA.w #.Table1_117
	CLC
	ADC $00
	STA $4342

	SEP #$20

	LDA.b #.Table2_117>>16
	STA $4334
	LDA.b #.Table1_117>>16
	STA $4344

	LDA #$18
	TSB $0D9F
	RTS

.Table1_117a
db $65,$3D,$4E
db $0E,$3D,$4F
db $0A,$3D,$50
db $02,$3E,$50
db $0A,$3E,$51
db $0B,$3E,$52
db $0B,$3E,$53
db $0A,$3E,$54
db $02,$3F,$54
db $0F,$3F,$55
db $80,$3F,$56
db $06,$3F,$56
db $00

.Table2_117a
db $61,$88
db $0C,$89
db $0A,$8A
db $09,$8B
db $08,$8C
db $09,$8D
db $08,$8E
db $09,$8F
db $0A,$90
db $0B,$91
db $80,$92
db $09,$92
db $00

.Table1_117
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4E
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$4F
db $01,$3D,$50
db $01,$3D,$50
db $01,$3D,$50
db $01,$3D,$50
db $01,$3D,$50
db $01,$3D,$50
db $01,$3D,$50
db $01,$3D,$50
db $01,$3D,$50
db $01,$3D,$50
db $01,$3E,$50
db $01,$3E,$50
db $01,$3E,$51
db $01,$3E,$51
db $01,$3E,$51
db $01,$3E,$51
db $01,$3E,$51
db $01,$3E,$51
db $01,$3E,$51
db $01,$3E,$51
db $01,$3E,$51
db $01,$3E,$51
db $01,$3E,$52
db $01,$3E,$52
db $01,$3E,$52
db $01,$3E,$52
db $01,$3E,$52
db $01,$3E,$52
db $01,$3E,$52
db $01,$3E,$52
db $01,$3E,$52
db $01,$3E,$52
db $01,$3E,$52
db $01,$3E,$53
db $01,$3E,$53
db $01,$3E,$53
db $01,$3E,$53
db $01,$3E,$53
db $01,$3E,$53
db $01,$3E,$53
db $01,$3E,$53
db $01,$3E,$53
db $01,$3E,$53
db $01,$3E,$53
db $01,$3E,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3E,$54
db $01,$3F,$54
db $01,$3F,$54
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$55
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $01,$3F,$56
db $00

.Table2_117
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$88
db $01,$89
db $01,$89
db $01,$89
db $01,$89
db $01,$89
db $01,$89
db $01,$89
db $01,$89
db $01,$89
db $01,$89
db $01,$89
db $01,$89
db $01,$8A
db $01,$8A
db $01,$8A
db $01,$8A
db $01,$8A
db $01,$8A
db $01,$8A
db $01,$8A
db $01,$8A
db $01,$8A
db $01,$8B
db $01,$8B
db $01,$8B
db $01,$8B
db $01,$8B
db $01,$8B
db $01,$8B
db $01,$8B
db $01,$8B
db $01,$8C
db $01,$8C
db $01,$8C
db $01,$8C
db $01,$8C
db $01,$8C
db $01,$8C
db $01,$8C
db $01,$8D
db $01,$8D
db $01,$8D
db $01,$8D
db $01,$8D
db $01,$8D
db $01,$8D
db $01,$8D
db $01,$8D
db $01,$8E
db $01,$8E
db $01,$8E
db $01,$8E
db $01,$8E
db $01,$8E
db $01,$8E
db $01,$8E
db $01,$8F
db $01,$8F
db $01,$8F
db $01,$8F
db $01,$8F
db $01,$8F
db $01,$8F
db $01,$8F
db $01,$8F
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$90
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$91
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $01,$92
db $00

level71:
level72:
level73:
level120:
	PHP
	REP #$20
	LDY #$02
	STY $4350
	LDY #$32
	STY $4351
	LDA.w #.Table1
	STA $4352
	LDY.b #.Table1>>16
	STY $4354
	SEP #$20
	LDA #$20
	TSB $0D9F
	PLP
	ldx #$01
	ldy #$01
	JSR L3UpLeftScroll
	RTS
.Table1
db $2D,$40,$80
db $09,$42,$83
db $0A,$43,$85
db $0A,$43,$86
db $09,$44,$87
db $0A,$45,$89
db $0A,$45,$8A
db $09,$46,$8C
db $0A,$47,$8D
db $0A,$47,$8F
db $0A,$48,$90
db $09,$49,$91
db $0A,$49,$93
db $0A,$4A,$94
db $09,$4A,$96
db $4C,$4B,$97
db $00

.Table2
db $2D,$20
db $13,$21
db $1D,$22
db $27,$23
db $1D,$24
db $5F,$25
db $00
level121:
	STZ $1413	; Set Horizontal Scroll Rate settings from Lunar Magic to "None."
	REP #$20
	LDA $1466
	SEC
	SBC #$FFFE
	STA $1466
	SEP #$20
	RTS

level12D:
LDA #$40
CMP $18E6
BCC Relative12D

LDA #$01
CMP $191B
BCC FlagIsOn12D


		    PHP
		    REP #%00110100
		    LDA $1C		; \
		    CMP.w #$0080	;  |  compare screen Y position
		    BCS VARH		;  |  with $C0 - desired Y
		    BCC ZERO		; /

ZERO:
		    LDA.w #$0000	; \
		    STA $24		;  |  set layer 3 Y to zero
		    PLP			;  |  if screen Y < the value
		    JMP SinkingCode12D

VARH:
		    LDA $1C		; \
		    SBC.w #$0080	;  |  set layer 3 Y
		    STA $24		;  |  to $C0 - desired height
		    PLP			;  |  if screen Y > or = the value



SinkingCode12D:
LDA $95
CMP #$06
BCC Return12D
INC $191B

FlagIsOn12D:

DEC $24
INC $18E6

LDA #$40
CMP $18E6
BNE Return12D

INC $18E6

Relative12D:
LDA #$01
CMP $75
BNE Return12D
LDA #$01
CMP $1908
BCC Return12D
INC $1908
JSL $80F60A

Return12D:			
			RTS
TablesTable:
db #Values1,#Values2,#Values3,#Values4,#Values5,#Values6				;Low byte

TablesTable2:
db #Values1>>8,#Values2>>8,#Values3>>8,#Values4>>8,#Values5>>8,#Values6>>8		;High byte

TablesTable3:
db #Values1>>16,#Values2>>16,#Values3>>16,#Values4>>16,#Values5>>16,#Values6>>16	;Bank byte

TableToUse:
db $00,$00,$00,$00,$00,$01,$01,$01,$01,$02,$02,$02,$02,$03,$03,$03,$03,$03,$03,$03

SmashSound:
;db $50,$50,$50,$50,$50,$20,$20,$20,$20,$20,$40,$40,$40,$10,$10,$10,$10,$10,$10,$10
db $50,$20,$40,$10,$40,$C0

!AnimationLastFrame = $146D ;1 byte

level12E:
lda $71
cmp !AnimationLastFrame
beq .dont_set_1DFC
sta !AnimationLastFrame
cmp #$00
beq +
cmp #$05
bcs +
	lda #$36
	sta $1DFC
	bra ++
+
	lda #$37
	sta $1DFC
++
.dont_set_1DFC

LDA $95
CMP #$0A
BNE DoNotTeleportToMidway

LDA #$06
STA $71
STZ $89
STZ $88

DoNotTeleportToMidway:


LDA $94
BNE DontLockCamera172
LDA #$03
STA $1412
DontLockCamera172:

REP #$20


LDA $96
CMP #$00B0
BCS DontLimit
SEP #$20
STZ $1412
BRA DoneLimiting


DontLimit:
SEP #$20
LDA #$01
STA $1412

DoneLimiting:

REP #$20
LDA $1A
LSR
LSR
STA $22
LDA $1C
LSR
LSR
STA $24
LDA $1A
LSR #7
SEP #$20
TAX
LDA TableToUse,X
STA $0E
REP #$20

LDA #$001C
STA $0C
LDA #$1468
STA $0A
JSR Smashers
RTS

	GetCurrentSongPositionIn44Time:
	REP #$20
	LDA $7FB004
	STA $4204
	SEP #$20
	LDA #$C0
	STA $4206
	NOP #8
	LDA $4216
	RTS
	

Smashers:
LDA $1DFB
CMP #$2A
BEQ AllowedToSmash
RTS
AllowedToSmash:
PHB
PHK
PLB 
SEP #$20
	LDA $0E

	TAX
	CMP #$03
	BNE UseNormalTable
	LDA $1F2A
	BNE UseNormalTable
	LDX #$02	

	UseNormalTable:
	LDA TablesTable,X
	STA $00
	LDA TablesTable2,X
	STA $01
	LDA TablesTable3,X
	STA $02

	JSR GetCurrentSongPositionIn44Time
	STA $7FC071
	TAX
	TAY
	REP #$20
	LDA ($00),y
	STA $04
	STA $00


	
	LDA $0C
	CMP #$001C
	LDA ($0C)
	BEQ IsVerticalSmasher
	SEC
	SBC $00
	AND #$00FF
	STA ($0A)
	BRA DoneDeterminingSmashers
IsVerticalSmasher:
	CLC
	ADC $00
	AND #$00FF
	STA ($0A)
DoneDeterminingSmashers:
	
	SEP #$20
	LDA FireValues,X
	STA $7FC070

	;REP #$20
	LDA $0E
	;LSR #7
	;SEP #$20
	TAX
	CMP #$03
	BNE UseNormalTable2
	LDA $1F2A
	BNE UseNormalTable2
	LDX #$02	

	UseNormalTable2:
	SEP #$20
	LDA $04
	CMP SmashSound,x
	BEQ MaybePlaySound	
Return:
	SEP #$20
	LDA $04
	STA $7F0E1E
PLB	
RTS


MaybePlaySound:
LDA $7F0E1E
CMP $04
BEQ Return
LDA #$09
STA $1DFC
STA $1887
BRA Return


Values1:
db	$00,$00,$00,$00,$00,$00,$00,$00,$03,$06,$09,$0C,$0F,$10,$10,$10
db	$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10
db	$10,$10,$10,$10,$10,$10,$10,$10,$18,$20,$28,$30,$38,$40,$48,$50
db	$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50

db	$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50
db	$4D,$4A,$47,$44,$41,$40,$40,$40,$38,$30,$28,$20,$18,$10,$08,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

Values2:
db	$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
db	$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
db	$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
db	$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20

db	$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20
db	$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$20,$18,$10,$08
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$08,$10,$08

Values3:
db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40
db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40
db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40
db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40

db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40
db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$38,$30,$28,$20,$18,$10,$08
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$08,$10,$18,$20,$28,$30,$38

Values4:
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$04,$08,$0C
db	$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10

db	$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10,$10
db	$0C,$08,$04,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

Values5:
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$10,$20,$30
db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40

db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40
db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40
db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40
db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40

db	$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$40,$30,$20,$10
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

Values6:
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$30,$60,$90
db	$C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0,$C0,$90,$60,$30

db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00


level138:
level1C9:
		LDA #$00
		STA $4330
		LDA #$02
		STA $4340

		LDA #$32
		STA $4331
		STA $4341

		REP #$20
		LDA.w #.138Table2
		STA $4332
		LDA.w #.138Table1
		STA $4342

		SEP #$20

		LDA.b #.138Table2>>16
		STA $4334
		LDA.b #.138Table1>>16
		STA $4344

		LDA #$18
		TSB $0D9F

		RTS

.138Table1
db $01,$3E,$57
db $04,$3E,$58
db $04,$3E,$59
db $03,$3E,$5A
db $03,$3E,$5B
db $04,$3E,$5C
db $04,$3E,$5D
db $04,$3E,$5E
db $09,$3E,$5F
db $08,$3D,$5F
db $05,$3C,$5F
db $08,$3B,$5F
db $32,$3A,$5F
db $05,$3A,$5E
db $02,$3A,$5D
db $03,$39,$5D
db $04,$39,$5C
db $04,$39,$5B
db $04,$39,$5A
db $06,$39,$59
db $0C,$39,$58
db $05,$3A,$58
db $04,$3B,$58
db $06,$3C,$58
db $05,$3D,$58
db $07,$3E,$58
db $19,$3F,$58
db $0A,$3F,$57
db $00

.138Table2
db $25,$96
db $0B,$97
db $07,$98
db $09,$99
db $08,$9A
db $06,$9B
db $05,$9C
db $08,$9D
db $08,$9E
db $50,$9F
db $05,$9E
db $03,$9D
db $04,$9C
db $03,$9B
db $04,$9A
db $04,$99
db $05,$98
db $02,$97
db $00


level142:
	LDA $9D		;\ if sprites are locked,
	ORA $13D4	; |
	BNE .Return	;/ branch

	LDA $13		;\  the 01 controls the scrolling speed
	AND #$03	; | the higher the number, the slower the scroll
	BNE .Return	;/  it needs to be one less than a power of 2

	STZ $1412	; disable vertical scroll so that SMW's scrolling code doesn't interfere

	STZ $55		; make sure that new tiles are uploaded from the top of the screen

	LDA $1464	;\
	ORA $1465	; | if the screen has reached the very top of the level,
	BEQ .Return	;/  branch
	LDA $1465
	CMP #$07	; Screen to stop scrolling at
	BCC .Return

	REP #$20	;\
	DEC $1464	; | decrease screen vertical position
	SEP #$20	;/

.Return
	RTS

level145:
JMP level103
RTS
;        LDA #$15    	;\
;	STA $212D   	;/add some layer to the subscreen
;	LDA $1462	;Load the layer 1 x position 
;	STA $22		;Store to the layer 3 x position 
;	LDA $1464	;Load the layer 1 y position
;	STA $24		;Store to the layer 3 y position	
;	RTS
level146:
	LDA $71
	CMP #$09
	BEQ Endo
	LDA $17
	AND #$30
	BNE St146
	LDA $7B
	CMP #$7E
	BCS St146
	LDA $7E
	CMP #$69
	BCC St146
	LDA #$01
	STA $1411
	BRA Endo
St146:	STZ $1411
Endo:	REP #$20			;\ Set processor 16 bit
	LDA #$0D02			;| $4330 = $210D
	STA $4330			;| $4331 = Mode 02
	LDA #$9E00			;| 
	STA $4332			;| Destination: $7F9E00
	LDY #$7F			;| (low and high byte)
	STY $4334			;| 
	LDA #$0F02			;| $4340 = $210F
	STA $4340			;| $4341 = Mode 02
	LDA #$9E00			;| 
	STA $4342			;| Destination: $7F9E00
	LDY #$7F			;| (low and high byte)
	STY $4344			;| 	
	SEP #$20			;/ Set processor 8 bit

	LDA #$18			;\ Enable HDMA
	TSB $0D9F			;/ on channel 3,4

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;HDMA Main Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	LDX #$00			;\ Init of
	LDY #$00			;/ X and Y

	SEP #$20			;\ Set processor 8 bit
	LDA $13				;| Set speed of waves
	LSR A				;| Adding more LSR A
	LSR A				;| will make it slower
	STA $00				;/ Store in scratch RAM

	PHB					;\ Push data bank
	PHK					;| Push program bank
	PLB					;/ Pull data bank

Wave_Loop_146:
	LDA #$06			;\ Set scanline height
	STA $7F9E00,x		;| for each wave
	TYA					;| Transfer Y to A
	ADC $00				;| Add in scratch RAM
	AND #$0F			;| and transfer #$0F bytes
	PHY					;| Push Y
	TAY					;/ Transfer A to Y

	LDA.w Wave_Table_146,y	;\ Load in wave values
	LSR A				;| half of waves only
	CLC					;| Clear carry flag
	ADC $1462			;| Apply to layer 1
	STA $7F9E01,x		;| X position low byte
	LDA $1463			;| And add nothing to
	ADC #$00			;| layer 1 X position
	STA $7F9E02,x		;/ high byte

	LDA.w Wave_Table_146,y	;\ Load in wave values
	CLC					;| Clear carry flag
	ADC $1466			;| Apply to layer 2
	STA $7F9E03,x		;| X position low byte
	LDA $1467			;| And add nothing to
	ADC #$00			;| layer 2 X position
	STA $7F9E04,x		;/ high byte

	PLY					;\ Pull Y
	CPY #$25			;| Compare with #$25 scanlines
	BPL End_Wave_146		;| If bigger, end HDMA
	INX					;| Increase X
	INX					;| Increase X
	INX					;| Increase X
	INY					;| Increase Y
	BRA Wave_Loop_146		;/ Do the loop

End_Wave_146:
	PLB					;\ Pull data bank
	LDA #$00			;| End HMDA by writing
	STA $7F9E05,x		;| #$00 here
	RTS			;/ Return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Table Settings
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Wave_Table_146:
	db $00
	db $01
	db $02
	db $03
	db $04
	db $05
	db $06
	db $07
	db $07
	db $06
	db $05
	db $04
	db $03
	db $02
	db $01
	db $00
RTS

level147:

	REP #$20	; 16 bit A
	LDA #$0000	; $43X0 = 00 
	STA $4330	; $43x1 = 00
	SEP #$20	; 8 bit A

	LDA $1F0E
	AND #$20
	BEQ Darken
	REP #$20	; 16 bit A
	LDA #$0C0F	; get pointer to brightness table
	BRA Lightn
Darken: REP #$20	; 16 bit A
	LDA #$0C00
Lightn:	STA $4332	; store it to low and high byte pointer
	PHK		; get bank
	PLY		;
	STY $4334	; store to bank pointer byte
	SEP #$20	; 8 bit A
	LDA #$08	; Enable HDMA on channel 3
	TSB $0D9F	;

	LDA $0F42
	CMP #$0F
	BNE Rtn147
	LDA #$29
	STA $1DFC
	LDA #$06
	STA $71
	STZ $89
	STZ $88
Rtn147:	RTS
level148:
	LDA $15
	AND #$08
	BEQ SkipU
	REP #$20
	INC $24
	SEP #$20
SkipU:	LDA $15
	AND #$04
	BEQ SkipD
	REP #$20
	DEC $24
	SEP #$20
SkipD:	RTS

level14C:
level14D:
level14E:
	STZ $1473
	RTS

level170:

LDA $95
CMP #$06
BNE DoNotChangeLevelNumber

LDA #$18
STA $13BF
BRA $05
DoNotChangeLevelNumber:
LDA #$52
STA $13BF

LDA $74
BEQ MarioIsntClimbing
LDA #$22
STA $13E0
MarioIsntClimbing:
	LDA $1B
	CMP #$06
	BEQ PlayFireSFX170
	CMP #$07
	BEQ PlayFireSFX170
	CMP #$0B
	BEQ PlayFireSFX170
	CMP #$0C
	BEQ PlayFireSFX170
	CMP #$12
	BEQ PlayFireSFX170
	CMP #$13
	BEQ PlayFireSFX170
	BRA DontPlayFireSFX170


PlayFireSFX170:
	LDA $7FC070
	CMP #$03
	BNE DontPlayFireSFX170
	LDA $7FC072
	CMP #$02
	BNE DontPlayFireSFX170

	LDA #$27
	STA $1DFC


	DontPlayFireSFX170:
	LDX #$00
	Loop170:
	CPX #$0D
	BEQ Return1701
	LDA $14C8,x
	CMP #$0B
	BEQ CheckSprite
	INX
	BRA Loop170

CheckSprite:
	LDA $9E,x
	CMP #$80
	BNE Return1701
	;LDA $167A,x
	;AND #$FB
	;STA $167A,x

	LDA $15
	AND #$77
	STA $15
	LDA $16
	AND #$77
	STA $16
	LDA $17
	AND #$70
	STA $17
	LDA $18
	AND #$70
	STA $18

	LDA $7D
	BPL Return1701
	LDA #$3F
	STA $7D

	Return1701:

	JSR GetCurrentSongPositionIn44Time
	TAX
	LDA $7FC070
	STA $7FC072
	LDA FireValues,X
	STA $7FC070
	LDA FireValues2,X
	STA $7FC071
	RTS
FireValues:
db	$03,$03,$03,$03,$03,$03,$03,$03,$04,$04,$04,$04,$04,$04,$04,$04
db	$03,$03,$03,$03,$03,$03,$03,$03,$04,$04,$04,$04,$04,$04,$04,$04
db	$03,$03,$03,$03,$03,$03,$03,$03,$04,$04,$04,$04,$04,$04,$04,$04
db	$03,$03,$03,$03,$03,$03,$03,$03,$04,$04,$04,$04,$04,$04,$04,$04

db	$03,$03,$03,$03,$04,$04,$04,$04,$03,$03,$03,$03,$04,$04,$04,$04
db	$03,$03,$03,$03,$04,$04,$04,$04,$03,$03,$03,$03,$04,$04,$04,$04
db	$03,$03,$03,$03,$04,$04,$04,$04,$03,$03,$03,$03,$04,$04,$04,$04
db	$05,$05,$05,$05,$05,$05,$05,$05,$06,$06,$06,$06,$06,$06,$06,$06

db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$01,$01,$01,$01,$01,$01,$01,$01,$02,$02,$02,$02,$02,$02,$02,$02

FireValues2:
db	$03,$03,$03,$03,$03,$03,$03,$03,$04,$04,$04,$04,$04,$04,$04,$04
db	$03,$03,$03,$03,$03,$03,$03,$03,$04,$04,$04,$04,$04,$04,$04,$04
db	$03,$03,$03,$03,$03,$03,$03,$03,$04,$04,$04,$04,$04,$04,$04,$04
db	$03,$03,$03,$03,$03,$03,$03,$03,$04,$04,$04,$04,$04,$04,$04,$04

db	$03,$03,$03,$03,$03,$03,$03,$03,$04,$04,$04,$04,$04,$04,$04,$04
db	$05,$05,$05,$05,$05,$05,$05,$05,$06,$06,$06,$06,$06,$06,$06,$06
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00

db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00
db	$01,$01,$01,$01,$01,$01,$01,$01,$02,$02,$02,$02,$02,$02,$02,$02
level171:
lda $71
cmp !AnimationLastFrame
beq .dont_set_1DFC
sta !AnimationLastFrame
cmp #$00
beq +
cmp #$05
bcs +
	lda #$36
	sta $1DFC
	bra ++
+
	lda #$37
	sta $1DFC
++
.dont_set_1DFC

STZ $13C8
REP #$20
LDA $1C
CMP #$0400
SEP #$20
BCS GoOn171
RTS
GoOn171:

;LDA $1462

;LDA $1463

;LDA $1464
;STA $D8,x
;LDA $1465

STZ $13F1

STZ $1411
REP #$20
;LDA $1C
;CMP #$
REP #$20
LDA $1C
STA $20
LDA #$001A
STA $0C
LDA #$1466
STA $0A
LDA $1C
CMP #$0980
BCC SecondSmasherType
LDA #$0004
BRA $03
SecondSmasherType:
LDA #$0005
STA $0E
JSR Smashers

REP #$20
LDA $96
CMP #$05C0
SEP #$20
BCC Return171
SEP #$20

LDA #$01
STA $1404

LDA $77
AND #$04
CMP #$04
BEQ Return171

LDA $1E00
BEQ CheckIfKeyIsNotPressed171
;LDA $0D9C
;BNE CanFly171





CanFly171:
STZ $1A
STZ $1B
STZ $1411

LDA $140D
BNE Return1712
LDA $15
AND #$80
CMP #$80
BNE Return1712
LDA $0D9C
BNE DontPlaySoundEffect171
LDA #$17
STA $1DFC
STA $0D9C
DontPlaySoundEffect171:

;LDA $7B
;BMI IsGoingLeft171

;LDA $15
;AND #$01
;BNE DontExtraDecSpeed171
;REP #$20
;LDA $94
;SEC
;SBC #$0001
;STA $94
;SEP #$20
;DontExtraDecSpeed171:
;DEC $7B
;BRA Done171

;IsGoingLeft171:
;LDA $76
;AND #$02
;BNE DontExtraIncSpeed171
;REP #$20
;LDA $94
;CLC
;ADC #$0001
;STA $94
;SEP #$20
;DontExtraIncSpeed171:
;INC $7B

Done171:
LDA #$01
STA $13C8
CLC
DEC $7D
DEC $7D
DEC $7D
DEC $7D
LDA $7D
BPL NoLimitYSpeed171
CMP #$D0
BCS NoLimitYSpeed171
LDA #$D0
STA $7D
NoLimitYSpeed171:
RTS

Return171:
STZ $1E00
STZ $0D9C

Return1712:
	RTS

CheckIfKeyIsNotPressed171:
LDA $15
AND #$80
CMP #$80
BEQ Return171
LDA #$01
STA $1E00
;STZ $0D9C
RTS
level172:
LDA $74
BEQ MarioIsntClimbing12E2
LDA #$22
STA $13E0
MarioIsntClimbing12E2:

	LDX #$00
Loop12E:
	CPX #$0D
	BEQ Return12E1
	LDA $14C8,x
	CMP #$0B
	BEQ CheckSprite12E2
	INX
	BRA Loop12E

CheckSprite12E2:
	LDA $9E,x
	CMP #$80
	BNE Return12E1
	;LDA $167A,x
	;AND #$FB
	;STA $167A,x

	LDA $15
	AND #$77
	STA $15
	LDA $16
	AND #$77
	STA $16
	LDA $17
	AND #$70
	STA $17
	LDA $18
	AND #$70
	STA $18

	LDA $7D
	BPL Return12E1
	LDA #$3F
	STA $7D

	Return12E1:
	LDA $94
	BMI LoadExit12E2
	LDA #$71
	STA $19B8
	LDA #$07
	STA $19D8
	RTS
	LoadExit12E2:
	LDA #$72
	STA $19B8
	LDA #$07
	STA $19D8

RTS

level179:
	LDA #$02
	STA $141E
	JMP Parallax

level17A:
	STZ $1411
REP #$20
LDA $1464
CMP #$0100
BCS Parallax

LDA #$0100
STA $1464

Parallax:
Parallax_INIT2:
	REP #$20	; 16 bit A
	LDA #$0F02	; $43X0 = 00 
	STA $4340	; $43x1 = 32
	SEP #$20	; 8 bit A
	LDA #$10	; Enable HDMA on channels 3
	TSB $0D9F
	LDA #$00
	STA $4342
	LDA #$B1
	STA $4343
	LDA #$7F
	STA $4344

	REP #$20
	INC $1466
	LDA $1466
	STA $7FB128
	STA $7FB149
	LSR A
	STA $7FB116
	STA $7FB125
	STA $7FB13A
	LSR A
	STA $7FB107
	STA $7FB110
	STA $7FB119
	STA $7FB11F
	STA $7FB131
	LSR A
	STA $7FB104
	STA $7FB10A
	STA $7FB11C
	STA $7FB12E
	STA $7FB134
	STA $7FB143
	LSR A
	STA $7FB10D
	STA $7FB122
	STA $7FB137
	STA $7FB140
Returnn:
	SEP #$20
	RTS

level187:
	STZ $15
	STZ $16
	STZ $17
	STZ $18
	RTS

level19E:
	LDA $40     ;\
	ORA #$04    ; |Set the layer 3 bit of CGADDSUB
	STA $40     ;/
        LDA #$13    ;\
	STA $212D   ;/add some layer to the subscreen

	LDA $9D ;\ If sprites locked,
	BNE .return

	LDA $14AF ;Load On/Off...
	CMP #$00 ;Check if on...
	BEQ .return ;If not, branch to off

	LDA $13D4	;pause fix
	BEQ .randomjump
	RTS
	.randomjump
	
	LDA $1869
	CMP #$01
	BNE .next
	STZ $16	
	STZ $17
	STZ $15	
	STZ $18	
	BRA .next
	.return
	RTS
	.next
	LDA $1864
	CMP #$00
	BNE .go
	LDA #$F0
	STA $1864
	LDA #$80
	STA $1DFB
	INC $1869
	
	.go
	LDA $1864
	CMP #$A0
	BCC .done
	DEC $1864
	RTS
	.done
	LDA $1864
	CMP #$80
	BCC .skip
	LDA #$1A
	STA $1DFC
	LDA $13
	AND #$06
	BNE .skip
	
	INC $1468
	
	.skip
	LDA $1864
	CMP #$50
	BCC .sjd
	DEC $1864
	RTS
	.sjd
	LDA #$56
	STA $1DFB
	
	STZ $1869
	
	LDA $1864
	CMP #$30
	BCC .ilied
	DEC $1864
	
	LDA $13
	AND #$07
	BNE .donenow
	
	REP #$20 ; Accum (16 bit)
	INC $1468
	SEP #$20

	LDA $13
	AND #$20
	BNE .donenow
	
	LDA #$1A
	STA $1DFC
	.donenow
	RTS
	.ilied
	LDA $1864
	CMP #$20
	BCC .morejumps
	DEC $1864
	
	LDA $13
	AND #$06
	BNE .ughjumps
	
	REP #$20 ; Accum (16 bit)
	INC $1468
	SEP #$20
	
	LDA $13
	AND #$1A
	BNE .ughjumps
	
	LDA #$1A
	STA $1DFC
	RTS
	.ughjumps
	RTS
	.morejumps
	LDA $1864
	CMP #$10
	BCC .makeitstop
	DEC $1864
	
	LDA $13
	AND #$05
	BNE .therehastobeabetterway
	
	REP #$20 ; Accum (16 bit)
	INC $1468
	SEP #$20
	
	LDA $13
	AND #$14
	BNE .therehastobeabetterway
	
	LDA #$1A
	STA $1DFC
	RTS
	.makeitstop
	LDA $13
	AND #$02
	BNE .therehastobeabetterway
	
	REP #$20 ; Accum (16 bit)
	INC $1468
	SEP #$20
	
	LDA $13
	AND #$0D
	BNE .therehastobeabetterway
	
	LDA #$1A
	STA $1DFC
	.therehastobeabetterway
	RTS

level19D:
	JSR levelA	

level1A1:
level1A2:
level1A7: 
level19F:
level1A0:
 	LDA $40     ;\
	ORA #$04    ; |Set the layer 3 bit of CGADDSUB
	STA $40     ;/
        LDA #$13    ;\
	STA $212D   ;/add some layer to the subscreen

	LDA $9D		;\ If sprites locked,
	BEQ GOGO	;/ branch
	RTS
GOGO:
	LDA $14AF	;Load On/Off...
	CMP #$00	;Check if on...
	BEQ Off		;If not, branch to off
Rising:
	LDA $1869	;
	CMP #$3F	;Load height RAM
	BEQ DoneG	;Branch if too high
	
NoSound:
	STZ $13D4			
	LDA #$FF 	;\ Just in case
	STA $13D3	;/

	LDA $1879	;
	CMP #$30	;Set delay timer
	BEQ NEXTA	;branch if ready

	INC $1879	;increase delay timer.
	RTS
NEXTA:

	INC $1864	;Increase timer

	LDA $1864	;
	CMP #$03	;See if timer is 4
	BNE ReturnG	;If not, branch

	LDA #$03	;Earthquake
	STA $1887	;
	LDA #$1A
	STA $1DFC

	INC $1869	;Increase HeightRAM
	STZ $1864	;Increase timer
	INC $1468	;Increase layer 2
	RTS
Off:
	LDA $1869	;
	CMP #$00	;Check height RAM.
	BEQ DoneG	;If zero, return.

	STZ $13D4			
	LDA #$FF ;\ Just in case
	STA $13D3;/


	LDA $1879	;
	CMP #$30	;Set delay timer
	BEQ NEXTOR	;branch if ready

	INC $1879	;increase delay timer.
	RTS
NEXTOR:


	INC $1864	;Increase timer

	LDA $1864	;
	CMP #$03	;See if timer is 4
	BNE ReturnG	;If so, branch

	LDA #$03	;Earthquake
	STA $1887	;
	LDA #$1A
	STA $1DFC

	DEC $1869	;Increase HeightRAM
	STZ $1864	;Increase timer
	DEC $1468	;Increase layer 2
ReturnG:
	RTS
DoneG: 	
	
YEEEAAAH:
	STZ $13D3
	STZ $1879
	
Turn_off:
	REP #$20
	LDA $7Fc0FC
	AND #$FFFE
	STA $7Fc0FC
	SEP #$20

	RTS		;Return

level1A4:
level1A3:
  	LDA $40     ;\
	ORA #$04    ; |Set the layer 3 bit of CGADDSUB
	STA $40     ;/
        LDA #$13    ;\
	STA $212D   ;/add some layer to the subscreen
	RTS

level1A5:
	JSR level1A3
	JSR Darkness

	LDA $9D		;\ If sprites locked,
	BNE .return

	LDA $14AF	;Load On/Off...
	CMP #$00	;Check if on...
	BEQ .return	;If not, branch to off

	LDA $1473
	CMP #$01
	BNE .keepgoing

	STZ $1864
	JMP FASTER

        LDA $13D4 ;pause fix 
        BEQ .randomjump 
        RTS 

.randomjump

.keepgoing
	LDA $1869
	CMP #$01
	BNE .next

	STZ $16
	STZ $17
	STZ $15
	STZ $18
	BRA .next
.return
	RTS
.next
	LDA $1864
	CMP #$00
	BNE .go
	LDA #$F0
	STA $1864
	LDA #$80
	STA $1DFB

	INC $1869 

.go
	LDA $1864
	CMP #$A0
	BCC .done
	DEC $1864
	RTS
.done
	LDA $1864
	CMP #$80
	BCC .skip
	LDA #$1A
	STA $1DFC
	LDA $13
	AND #$06
	BNE .skip

	REP #$20
	DEC $1468
	SEP #$20	

.skip
	LDA $1864
	CMP #$50
	BCC .sjd
	DEC $1864
	RTS
.sjd
	LDA #$56
	STA $1DFB

	STZ $1869

	LDA $1864
	CMP #$30
	BCC .ilied
	DEC $1864

	LDA $13
	AND #$07
	BNE .donenow

        REP #$20                  ; Accum (16 bit) 
	DEC $1468
	SEP #$20

	LDA $13
	AND #$20
	BNE .donenow

	LDA #$1A
	STA $1DFC
.donenow
	RTS
.ilied	
	LDA $1864
	CMP #$20
	BCC .morejumps
	DEC $1864

	LDA $13
	AND #$06
	BNE .ughjumps

        REP #$20                  ; Accum (16 bit) 
	DEC $1468
	SEP #$20

	LDA $13
	AND #$1A
	BNE .ughjumps

	LDA #$1A
	STA $1DFC
	RTS
.ughjumps
	RTS
.morejumps
	LDA $1864
	CMP #$10
	BCC .makeitstop
	DEC $1864

	LDA $13
	AND #$05
	BNE .therehastobeabetterway

        REP #$20                  ; Accum (16 bit) 
	DEC $1468
	SEP #$20

	LDA $13
	AND #$14
	BNE .therehastobeabetterway

	LDA #$1A
	STA $1DFC
	RTS
.makeitstop
	LDA $13
	AND #$01
	BNE .therehastobeabetterway

        REP #$20                  ; Accum (16 bit) 
	DEC $1468
	SEP #$20

	LDA $13
	AND #$0D
	BNE .therehastobeabetterway

	LDA #$1A
	STA $1DFC

.therehastobeabetterway
	RTS
FASTER:
	LDA $13
	AND #$01
	BNE .donewhoop

        REP #$20                  ; Accum (16 bit) 
	DEC $1468
	DEC $1468
	SEP #$20

	LDA $13
	AND #$09
	BNE .donewhoop

	LDA #$1A
	STA $1DFC
.donewhoop
	RTS

level1A8:
;	JSR level1A3
;	JMP level102
RTS


;water is poisonous if ON/OFF switch is ON
PoisonWater:
	LDA $14AF
	EOR #$01
	AND $75
	BEQ +
	JSL $80F5B7
	+

	RTS

Submerged:
	LDA $71
	CMP #$09
	BEQ Return66
	LDA $9D
	BNE Return66

	LDA $75
	CMP #$01
	BEQ InWater
	RTS
InWater:
	LDA #$0E	; hide player's bottom tile and swimming tiles
	STA $78
Return66:
	RTS

level1D:

	REP #$20			
	LDA.w #.parallax_table		
	STA $00					
	SEP #$20			
	LDA.b #.parallax_table>>16	
	STA $02	
	LDA #$01
	STA !useParallaxScroll
	JMP DoParallaxScrolling

.parallax_table:
dw $0000, $0018
dw $0057, $0018
dw $00AF, $0028
dw $00FF, $0020
dw $010E, $0018
dw $015A, $0018

DoParallaxScrolling:
	REP #$20			
	LDA $00
	CLC				
	ADC #$0002			
	STA $03				
	CLC				
	ADC #$0002			
	STA $06				
						
	SEP #$20
	LDA $02
	STA $05				
	STA $08				

	
	JSR DoScrollStuff
	PHB
	PHK
	PLB
	LDX #$00			; Y = index to the offset table
	LDY #$00			; X = index to the HDMA table in RAM
					; We don't need to load #$0000 since when X and Y
	
	
.loop
	REP #$30			;
	LDA [$00],y			; \
	BMI .exitLoop			; | (Negative values are table ends)
	SEC				; | If the current y offset
	SBC !cameraY			; | Is above the screen or at the screen
	BMI .base			; | Then set the first scanline and beyond to that y position's x offset
	BEQ .base			; /

	INX				; \ Increase the HDMA table position
	INX				; |
	INX				; /

	LDA [$03],y			; \
	JSR DoCalculation		; | Get the offset and do the requested calculation with it
	STA !FreeRAM+1,x		; / And store it to the current slot in the HDMA table
	
	
	LDA [$06],y			; \
	SEC				; | Get the difference in scanline counts.
	SBC [$00],y			; | And store it to the current slot in the HDMA table.
	SEP #$20			; |
	STA !FreeRAM,x			; /
	
	
	INY				; \
	INY				; | Increase the offset table position
	INY				; |
	INY				; /
	BRA .loop			;
					;
.base					;
	LDA [$03],y			; \
	JSR DoCalculation		; | Get the offset and do the requested calculation with it
	STA !FreeRAM+1,x		; / And store it to the current slot in the HDMA table
	
	LDA [$06],y			; \
	SEC				; | Get the difference in scanline counts.
	SBC !cameraY			; | But only if it's positive
	BMI +				; |
	SEP #$20			; |
	STA !FreeRAM,x			; /

+
	INY				; \
	INY				; | Increase the offset table position
	INY				; | We don't increase the offset table position,
	INY				; / since we might jump here more than once.
	BRA .loop			;

.exitLoop
	SEP #$20
	LDA #$7F
	STA !FreeRAM,x
	LDA !FreeRAM-2,x
	STA !FreeRAM+1,x
	LDA !FreeRAM-1,x
	STA !FreeRAM+2,x
	LDA #$00
	STA !FreeRAM+3,x
	SEP #$10
	
	PLB
	RTS

DoCalculation:
	CMP #$0100
	BEQ +
	SEP #$20
	STA $4206
	REP #$20
	LDA $1A
	ASL
	ASL
	ASL
	STA $4204
	NOP #8
	LDA $4214
	RTS
+
	LDA #$0000
	RTS

DoScrollStuff:	
	LDA #$02		; \ One register, write twice
	STA $4360		; /

	LDA #$0F		; \ Write to the layer 2 horizontal position
	STA $4361		; /
	
	LDA.b #!FreeRAM
	STA $4362
	LDA.b #!FreeRAM>>8
	STA $4363
	LDA.b #!FreeRAM>>16
	STA $4364	

	LDA #$40
	TSB $0D9F
+
	RTS

!L3UpLeftScroll_X_acc_bits = $140B
!L3UpLeftScroll_Y_acc_bits = $140C
!L3UpLeftScroll_X_position = $1869
!L3UpLeftScroll_Y_position = $145E

; X = X speed
; Y = Y speed
; Speeds only go to the left
L3UpLeftScroll:
	lda $9D
	ora $13D4
	bne .return
	lda #$01
	sta $13D5
	lda #$00
	xba
	txa
	clc
	adc !L3UpLeftScroll_X_acc_bits ; This is cleared on level load. Also used with hindenbird. L3 X offset
	-
	cmp #$10
	bcc +
		rep #$20
		inc !L3UpLeftScroll_X_position
		sep #$20
		sec
		sbc #$10
		bra -
	+
	sta !L3UpLeftScroll_X_acc_bits
	tya
	clc
	adc !L3UpLeftScroll_Y_acc_bits ; This is cleared on level load. Also used with hindenbird. L3 X offset
	-
	cmp #$10
	bcc +
		rep #$20
		inc !L3UpLeftScroll_Y_position
		sep #$20
		sec
		sbc #$10
		bra -
	+
	sta !L3UpLeftScroll_Y_acc_bits
	.draw
	php
	stz $00
	stz $01
	jsr sub_layer_3_scroll
	rep #$20
	lda !L3UpLeftScroll_X_position
	and #$01FF
	clc
	adc $22
	STA $22

	lda !L3UpLeftScroll_Y_position
	and #$01FF
	clc
	adc $24
	STA $24
	plp
	.return
	rts


!make_particles_cluster_sprite_num = $0F		; Cluster sprite number

!ParticleXPos = $1E16
!ParticleYPos = $1E02
!ParticleXSpeed1 = $1E66
!ParticleXSpeed2 = $0F72
!ParticleXSpeedFraction = $1E8E
!ParticleYSpeed = $1E52
!ParticleYSpeedFraction = $1E7A
!ParticleProps = $0F86
!ParticleSize = $0F5E
!ParticleTile = $1E3E
!ParticleShow = $1E2A
!ParticleTimeToLive = $0F9A

!make_particles_start_positions_pointer = $00
!make_particles_y_speed_table_pointer = $02
!make_particles_x_speed_1_table_pointer = $04
!make_particles_x_speed_2_table_pointer = $06
!make_particles_tile = $08
!make_particles_size = $09
!make_particles_props = $0A
; y = number of particles to spawn
make_particles:
php
-

lda #!make_particles_cluster_sprite_num
sta $1892,y

lda ($00),y	; \ Initial X and Y position of each sprite.
pha 		; | Is relative to screen border.
and #$F0 	; |
sta !ParticleXPos,y 	; |

pla 		; |
asl #4		; |
sta !ParticleYPos,y 	; |

lda ($02),y
sta !ParticleYSpeed,y

lda ($04),y
sta !ParticleXSpeed1,y

lda ($06),y
sta !ParticleXSpeed2,y

lda !make_particles_tile
sta !ParticleTile,y

lda !make_particles_size
sta !ParticleSize,y

lda !make_particles_props
sta !ParticleProps,y

lda #$00
sta !ParticleYSpeedFraction,y
sta !ParticleXSpeedFraction,y
sta !ParticleTimeToLive,y
sta !ParticleShow,y

dey 		; | Loop until all slots are done.
bpl - 		; /


lda #$01 	; \ Run cluster sprite routine.

sta $18B8 	; /


plp
rts              ; Return.


!FlowerTile = $67	;Tile # of the flower petal. was $67
!FlowerSize = $00	;Size of flower petal. 8x8 by default
!FlowerProps = $7A	;YXPPCCCT of tile. XY flip handled by Properties table.;was 35, 3F
!FlowerCount = $09			; Amount of sprites to fall down, -1. Values outside of 00-13 are not recommended.
make_flowers_default:
	php
	rep #$20
	lda #.init_xy
	sta !make_particles_start_positions_pointer
	lda #.y_speeds
	sta !make_particles_y_speed_table_pointer
	lda #.x_speeds_1
	sta !make_particles_x_speed_1_table_pointer
	lda #.x_speeds_2
	sta !make_particles_x_speed_2_table_pointer
	sep #$20

	lda #!FlowerTile
	sta !make_particles_tile

	lda #!FlowerSize
	sta !make_particles_size

	lda #!FlowerProps
	sta !make_particles_props

	ldy #!FlowerCount
	jsr make_particles
	plp
	rts

	.init_xy
		db $06,$45,$9E,$E2,$A7,$BC,$59,$40,$61,$F5,$D6,$24,$7B,$33,$C6,$0B,$00,$39,$70,$A1
	.y_speeds
		db $10,$20,$10,$20,$10,$20,$10,$20,$10,$20,$10,$20,$10,$20,$10,$20,$10,$20,$10,$20
	.x_speeds_1
		db $D0,$F0,$E0,$F0,$D0,$F0,$E0,$F0,$D0,$F0,$E0,$F0,$D0,$F0,$E0,$F0,$D0,$F0,$E0,$F0 
	.x_speeds_2
		db $F0,$E0,$D0,$E0,$F0,$E0,$D0,$E0,$F0,$E0,$D0,$E0,$F0,$E0,$D0,$E0,$F0,$E0,$D0,$E0



!SandTile = $65		;Tile # of the sand tile.
!SandSize = $00		;Size of sand tile. 8x8 by default
!SandProps = $3A	;YXPPCCCT of tile. was 30
!SandCount = $13
make_sandstorm_default:
	php
	rep #$20
	lda #.init_xy
	sta !make_particles_start_positions_pointer
	lda #.y_speeds
	sta !make_particles_y_speed_table_pointer
	lda #.x_speeds
	sta !make_particles_x_speed_1_table_pointer
	lda #.x_speeds
	sta !make_particles_x_speed_2_table_pointer
	sep #$20

	lda #!SandTile
	sta !make_particles_tile

	lda #!SandSize
	sta !make_particles_size

	lda #!SandProps
	sta !make_particles_props

	ldy #!SandCount
	jsr make_particles
	plp
	rts

	.init_xy
		db $06,$45,$9E,$E2,$A7,$BC,$59,$40,$61,$F5,$D6,$24,$7B,$33,$C6,$0B,$00,$39,$70,$A1
	.y_speeds
		db $10,$20,$10,$20,$10,$20,$10,$20,$10,$20,$10,$20,$10,$20,$10,$20,$10,$20,$10,$20
	.x_speeds
		db $90,$B0,$A0,$B0,$90,$B0,$A0,$B0,$90,$B0,$A0,$B0,$90,$B0,$A0,$B0,$90,$B0,$A0,$B0

!RainTile = $2D	;Tile # of the rain.
!RainSize = $01	;Size of rain tile. 16x16 by default
!RainProps = $36 ;Tile property of the rain.
!RainCount = $13
make_rain_default:
	php
	rep #$20
	lda #.init_xy
	sta !make_particles_start_positions_pointer
	lda #.y_speeds
	sta !make_particles_y_speed_table_pointer
	lda #.x_speeds
	sta !make_particles_x_speed_1_table_pointer
	lda #.x_speeds
	sta !make_particles_x_speed_2_table_pointer
	sep #$20

	lda #!RainTile
	sta !make_particles_tile

	lda #!RainSize
	sta !make_particles_size

	lda #!RainProps
	sta !make_particles_props

	ldy #!RainCount
	jsr make_particles
	plp
	rts

	.init_xy
		db $06,$45,$9E,$E2,$A7,$BC,$59,$40,$61,$F5,$D6,$24,$7B,$33,$C6,$0B,$00,$39,$70,$A1
	.y_speeds
		db $50,$50,$50,$50,$50,$05,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50,$50
	.x_speeds
		db $F0,$E0,$F0,$E0,$F0,$E0,$F0,$E0,$F0,$E0,$F0,$E0,$F0,$E0,$F0,$E0,$F0,$E0,$F0,$E0

!AshTile = $65	;Tile # of the ash
!AshSize = $00	;Size of ash. 8x8 by default
!AshProps = $7A	;YXPPCCCT of tile. XY flip handled by Properties table.;was 35, 3F
!AshCount = $13			; Amount of sprites to exist down, -1. Values outside of 00-13 are not recommended.
!AshTTLMin = $C0
!AshTTLMax = $F0
function sub(a,b) = a-b
!AshMod = sub(!AshTTLMax, !AshTTLMin)
do_ashes:
	php
	ldy #!AshCount

	-
	lda $1892,y
	bne .dont_make
	lda #!make_particles_cluster_sprite_num
	sta $1892,y

	lda .init_xy,y	; \ Initial X and Y position of each sprite.
	pha 		; | Is relative to screen border.
	and #$F0 	; |
	sta !ParticleXPos,y 	; |

	pla 		; |
	asl #4		; |
	sta !ParticleYPos,y 	; |

	lda .y_speeds,y
	sta !ParticleYSpeed,y

	lda .x_speeds_1,y
	sta !ParticleXSpeed1,y

	lda .x_speeds_2,y
	sta !ParticleXSpeed2,y

	lda #!AshTile
	sta !ParticleTile,y

	lda #!AshSize
	sta !ParticleSize,y

	lda #!AshProps
	sta !ParticleProps,y

	lda #$00
	sta !ParticleYSpeedFraction,y
	sta !ParticleXSpeedFraction,y
	sta !ParticleShow,y

	jsl $01ACF9 ; Get Rand
	lda $148D

	cmp.b #!AshMod
	bcc +
		sec
		sbc.b #!AshMod
	+
	clc
	adc.b #!AshTTLMin
	sta !ParticleTimeToLive,y

	.dont_make
	dey 		; | Loop until all slots are done.
	bpl - 		; /

	lda #$01 	; \ Run cluster sprite routine.
	sta $18B8 	; /

	plp
	rts
	.init_xy
		db $06,$45,$9E,$E2,$A7,$BC,$59,$40,$61,$F5,$D6,$24,$7B,$33,$C6,$0B,$00,$39,$70,$A1
	.y_speeds
		db $F8,$F0,$F8,$F0,$F8,$F0,$F8,$F0,$F8,$F0,$F8,$F0,$F8,$F0,$F8,$F0,$F8,$F0,$F8,$F0
	.x_speeds_1
		db $F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8,$F8
	.x_speeds_2
		db $08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08,$08
